export const HotelDetails = {

  computed: {
    domainDetails() {
      return this.$store.getters.domainDetails;
    },
    
    isDeactive() {
      return this.$store.getters.isDeactive;
    },

    hotel() {
      return this.$store.getters.hotel;
    },

    landingpage() {
      return this.$store.getters.landingpage;
    },

    stylecode() {
      return this.$store.getters.domainStyle;
    },

    language() {
      return this.$store.getters.getLanguage;
    },

    activepage() {
      return this.$store.getters.activePage;
    },

    currency() {
      return this.$store.getters.getCurrency;
    },

    withoutOfferVal() {
      return this.$store.getters.getWithoutOffer;
    },

    bookNowVal() {
      return this.$store.getters.getBookNow;
    },
    
    bidDetail() {
      return this.$store.getters.getBidDetail;
    },
    
    selectedBidRoom() {
      return this.$store.getters.selectedBidRoom;
    },

    domain() {
      return this.$store.getters.domain;
    },
    
    groupId() {
      return this.$store.getters.getGroupId;
    },

    banner() {
      return this.$store.getters.getBanner;
    },
    
    isCancel() {
      return this.$store.getters.getIsCancel;
    },

    dayBanner() {
      return this.$store.getters.getDayBanner;
    },
    bannerUseType() {
      return this.$store.getters.getBannerUseType;
    },

    isGuestMsg() {
      let count = 0;
      let rooms = this.$store.getters.selectedBidRoom;
      Object.keys(rooms).forEach(function(key) {
          if(rooms[key].room_searched_for > rooms[key].hotel_rate_for) {
              count += rooms[key].count*(rooms[key].room_searched_for-rooms[key].hotel_rate_for);
          }
      });
      if(count == 0)
          return false;
      else 
          return true;
    },
    
  }
  
}

