// import router from '@/router.js'

export const BidDetails = {
    
    computed: {
        
    },
    
    methods: {

        setBidStorage(data) {
            console.log('data---',data);
            this.$store.commit('setBidStatus', data.status);
            this.$store.commit('setBidCountOffer', data.countOffer);
        },
        
        searchbidSubmit( bidDetail, type='') {
            this.$store.commit('setPreviousBidAmt', bidDetail.pre_bid_amount_daily);
            bidDetail['is_home'] = this.$store.getters.getIsHome;
            
            let apiURL = '/' + this.language + '/search-submit';

            this.$axios.post(apiURL, bidDetail)
            .then((response) => {
                console.log('lll'+response)
                let data = response.data.data;
                
                this.$store.commit('setIsHome', 0);

                if(data.redirect_to == 'home') {

                    console.log('homeError', laravelError);

                    this.$toastr.s(this.$t('message.NO_ROOM_LEFT'));
                    this.$router.push({path: '/'}); return;
                }
                
                if(data.booking_offer_status == 'intransaction' && data.payment_gateway == true) {
                    this.setBidStorage( data );
                    this.$store.commit('setBookingId', data.booking_id);
                    this.$store.commit('setBookingDetail', data.booking_detail);
                    this.$store.commit('setBookingUserEmail', data.booking_user_email);
                    this.$store.commit('setPaymentToken', data.Token);
                    // this.$router.push({path: data.redirect_to});
                    window.location.href = data.redirect_to; 
                    return;

                } else if(data.booking_offer_status == 'intransaction' || data.booking_offer_status == 'accepted') {
                  
                    this.setBidStorage( data );
                    this.$store.commit('setBookingId', data.booking_id);
                    this.$store.commit('setBookingDetail', data.booking_detail);
                    this.$store.commit('setBookingUserEmail', data.booking_user_email);
                    
                    this.$router.push({path: data.redirect_to});
                    
                } else if(data.booking_offer_status == 'rejected' && data.OfferReject == false) {
                    
                    this.setBidStorage( data );
                    this.$store.commit('setNewBiddingPrice', data.newBiddingPrice);
                    this.$store.commit('setBookingId', data.booking_id);
                    
                    if(data.finalBidPriceMsg != undefined && data.finalBidPriceMsg)
                        this.$store.commit('setFinalBidPriceMsg', data.finalBidPriceMsg);
                    
                    this.$router.push({path: data.redirect_to});
                    
                } else if(data.OfferReject == true && data.limitOver) {
                    
                    let selectedBidDetail = bidDetail.selected_search_id;
                    let selectedBidIndex = this.$store.getters.getLimitExceedRoom;
                    let plimit_exceed_room = this.$store.getters.getPLimitExceedRoom;
                    this.$store.commit('setPreviousBidAmt', 0);
                    if(selectedBidIndex){
                        selectedBidIndex = selectedBidIndex+',';
                        // selectedBidIndex = selectedBidIndex+','+plimit_exceed_room;
                    }else{
                        selectedBidIndex = '';
                        // selectedBidIndex = plimit_exceed_room;
                    }
                    if(selectedBidDetail){
                        let selectedBidDetailArr = selectedBidDetail.split('-');
                        selectedBidIndex = selectedBidIndex+selectedBidDetailArr[1];
                    }
                    this.setBidStorage( data );
                    this.$store.commit('setNewBiddingPrice', data.newBiddingPrice);

                    this.$store.commit('setLimitExceedRoom', selectedBidIndex);

                    this.$router.push({path: data.redirect_to});
                    
                } else {
                    
                    // console.log('apiFailError', laravelError);

                    this.$toastr.s(this.$t('message.NO_ROOM_LEFT'));

                    window.location.href = '/'

                }
                
            })
            .catch((error) => {

                var laravelError = error.response.data;

                console.log('catchError', laravelError);

                // this.$toastr.s(this.$t('message.NO_ROOM_LEFT'));

                if(laravelError.code.code == 422) {
                    /*let token = this.$store.getters.token;
                    if(token) this.$store.commit('setToken', null);*/
                }
                window.location.href = '/';
                
            });

        },

        initialOffer( bidDetail, type='') {
            
            let apiURL = '/' + this.language + '/initial-offer';

            this.$axios.post(apiURL, bidDetail)
            .then((response) => {

                let data = response.data.data; 

            })
            .catch((error) => {

            });

        }

    }

}

