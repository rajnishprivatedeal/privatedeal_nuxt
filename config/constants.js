import Locales from '../localization.json'

export const Constants = {

  // Default language 
  defaultLanguage: 'en',

  // API endpoint
  apiEndPoint: '/api',

  // Base URL
  baseURL: '',
  
  productionTip: false,

  // VeeValidate validation message dictionary
  i18n: {
    en: { attributes: Locales.en.validation["attributes"] },
    fr: { attributes: Locales.fr.validation["attributes"] },
    de: { attributes: Locales.de.validation["attributes"] },
    es: { attributes: Locales.es.validation["attributes"] }
  },

  /*Production variable*/

 /* httpProtocol: 'https://',
  hostName: 'privatedeal.com',
  filePath : 'https://d2ii27ilnri92x.cloudfront.net',*/

  /*Staging variable*/
  
  // httpProtocol: 'https://',
  // hostName: 'staging-privatedeal.com',
  // filePath : 'https://d3cfbhpm32okc.cloudfront.net',

  /*Local variable*/

 
  httpProtocol: 'http://',
 // // hostName: 'localhost:8082',
  hostName: 'pvtdeal.com',
  filePath : 'https://d3cfbhpm32okc.cloudfront.net',

}
