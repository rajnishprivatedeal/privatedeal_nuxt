import { resolve } from 'path'
import Locales from './localization.json'

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'server',
  ssr:false,

 

  alias: {
    'style': resolve(__dirname, './assets/style')
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'privatedeal_nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      },
    ],

    script: [
    {
        src: "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
        type: "text/javascript"
      },
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js",
        type: "text/javascript"
      },
      {
        src:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",
        type: "text/javascript"
      }
    // { src:"/js/jquery.min.js",type:'text/javascript'},
      // { src:"/js/popper.min.js",type:'text/javascript'},
      // { src:"/js/bootstrap.min.js",type:'text/javascript'},
      

    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  //'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'
  css: [
    '~/assets/css/bootstrap.min.css',
    '~/assets/css/style.css', 
    '~/assets/css/media.css',    
  ],




  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/vuex-persist', ssr: false },
    { src: '~/plugins/axios', ssr: false },
    // { src: '~/plugins/localStorage.js', ssr: false }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  '@nuxt/components',
  '@nuxtjs/router',
  '@nuxtjs/router-extras',
  '@nuxtjs/moment',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/axios',
    'nuxt-i18n',
    '@nuxtjs/proxy',
  ],

  i18n: {
    locales: ['de','en', 'fr', 'es'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: Locales
    }
  },

  axios: {  
    //baseURL: "http://lofthotelbordeaux.pvtdeal.com/api",
    //baseURL: "http://lafleurdulac.pvtdeal.com/api",
    //baseURL: "http://lofthotels.pvtdeal.com/api",
    // credentials: false,
    // proxy:true
},
proxy: {
   // '/api/': 'http://lofthotelbordeaux.pvtdeal.com',
   // '/api/': 'http://lafleurdulac.pvtdeal.com',
  '/api/':'http://lofthotels.pvtdeal.com',
  },

dev: process.env.NODE_ENV !== 'production',

env: {
    baseURL: process.env.BASE_URL
  },

  publicRuntimeConfig: {
    browserBaseURL: process.env.BROWSER_BASE_URL
  },
  privateRuntimeConfig: {
    apiSecret: process.env.API_SECRET
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    
    extend(config, ctx) {
      config.resolve.alias.vue$ = require.resolve('vue/dist/vue.esm.js')
    }
  },
  server: {
    port: 8084 // default: 3000
  },
  generate: {
    routes: [
      '/'
    ]
  }
}
