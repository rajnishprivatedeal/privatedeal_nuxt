import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/home/index'
import UserProfile from '@/components/user-profile/Index'
import HotelList from '@/components/hotel-list/Index'
// import HotelSearch from '@/components/HotelSearch'
// import Offer from '@/components/Offer'
import FAQ from '@/components/faq'
import Terms from '@/components/TermsAndConditions'
// import Contact from '@/components/ContactUs'
import Policy from '@/components/PrivacyPolicy'
import CookiePolicy from '@/components/CookiePolicy'
// import RejectOffer from '@/components/RejectOffer'
import GuestUserBookingDetail from '@/components/guest-user-detail/Index'
import HotelBooking from '@/components/white-brand-bid/Booking'
// import CancelOffer from '@/components/white-brand-bid/CancelOffer'
import CreditCard from '@/components/white-brand-bid/CreditCard'
// import PaymentError from '@/components/white-brand-bid/PaymentError'
// import PaymentFail from '@/components/white-brand-bid/PaymentFail'
import PaymentSuccess from '@/components/white-brand-bid/PaymentSuccess'
// import NoRoomError from '@/components/white-brand-bid/NoRoomError'
// import ResetPassword from '@/components/resetPassword'
// import VerifyEmail from '@/components/verifyEmail'
import HotelPolicy from '@/components/hotelPolicy'

Vue.use(Router)


export function createRouter() {
return new Router({
  mode: 'history',
  // base: __dirname, 
  routes: [
    /*-----routing for search hotel rooms------*/
    {
      path: '/night-use-search',
      name: 'night-use-search',
      component: HotelList
    },
    {
      path: '/day-use-search',
      name: 'day-use-search',
      component: HotelList
    },
    /*-----routing for search hotel rooms------*/
    
    /*-----routing for guest-booking-detail------*/
    {
      path: '/day-use-guest-booking-detail',
      name: 'day-use-guest-booking-detail',
      component: GuestUserBookingDetail
    },
    {
      path: '/night-use-guest-booking-detail',
      name: 'night-use-guest-booking-detail',
      component: GuestUserBookingDetail
    },
    // /*-----routing for guest-booking-detail------*/
    
    // //routing for bidding accepted or rejected
    {
      path: '/day-use-first-trial',
      name: 'day-use-first-trial',
      component: HotelBooking
    },
    {
      path: '/night-use-first-trial',
      name: 'night-use-first-trial',
      component: HotelBooking
    },
    {
      path: '/day-use-second-trial',
      name: 'day-use-second-trial',
      component: HotelBooking
    },
    {
      path: '/night-use-second-trial',
      name: 'night-use-second-trial',
      component: HotelBooking
    },
    {
      path: '/day-use-third-trial',
      name: 'day-use-third-trial',
      component: HotelBooking
    },
    {
      path: '/night-use-third-trial',
      name: 'night-use-third-trial',
      component: HotelBooking
    },
    {
      path: '/day-use-final-offer-price',
      name: 'day-use-final-offer-price',
      component: HotelBooking
    },
    {
      path: '/night-use-final-offer-price',
      name: 'night-use-final-offer-price',
      component: HotelBooking
    },
    {
      path: '/day-use-offer-rejected',
      name: 'day-use-offer-rejected',
      component: HotelBooking
    },
    {
      path: '/night-use-offer-rejected',
      name: 'night-use-offer-rejected',
      component: HotelBooking
    },
    {
      path: '/day-use-offer-accepted',
      name: 'day-use-offer-accepted',
      component: HotelBooking
    },
    {
      path: '/day-use-accept',
      name: 'day-use-accept',
      component: HotelBooking
    },
    // {
    //   path: '/offer-cancel',
    //   name: 'offer-cancel',
    //   component: CancelOffer
    // },
    {
      path: '/night-use-offer-accepted',
      name: 'night-use-offer-accepted',
      component: HotelBooking
    },
    {
      path: '/booking-select-card',
      name: 'booking-select-card',
      component: CreditCard
    },
    // {
    //   path: '/payment-error',
    //   name: 'payment-error',
    //   component: PaymentError
    // },
    // {
    //   path: '/payment-fail',
    //   name: 'payment-fail',
    //   component: PaymentFail
    // },
    {
      path: '/payment-successful',
      name: 'payment-successful',
      component: PaymentSuccess
    },
    // {
    //   path: '/no-room-error',
    //   name: 'no-room-error',
    //   component: NoRoomError
    // },
    // //routing for bidding accepted or rejected
    {
      path:'/profile',
      name:'profile',
      component:UserProfile
    },
    // {
    //   path: '/Offer',
    //   name: 'Offer',
    //   component: Offer
    // },
    {
      path: '/FAQ',
      name: 'faq',
      component: FAQ
    },
    {
      path: '/:locale/TCwhite',
      name: 'terms',
      component: Terms
    },
    {
      path: '/:locale/PPwhite',
      name: 'policy',
      component: Policy
    },
    {
      path: '/:locale/CPwhite',
      name: 'cookie',
      component: CookiePolicy
    },
    // {
    //   path: '/contact-us',
    //   name: 'contactus',
    //   component: Contact
    // },
    // {
    //   path: '/password/reset/:token',
    //   name: 'resetpassword',
    //   component: ResetPassword
    // },
    // {
    //   path: '/verify-email/:locale?/:token?',
    //   name: 'verify-email',
    //   component: VerifyEmail
    // },
    {
      path: '/hotel-policy',
      name: 'hotel-policy',
      component: HotelPolicy
    },
    {
      path: '/:locale?',
      name: 'welcome',
      component: require('./components/home/Index.vue').default
    },
  ]
})
}
