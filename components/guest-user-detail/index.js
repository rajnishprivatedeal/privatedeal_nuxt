  // import Header from  '@/components/header/header.vue';
  // import Footer from  '@/components/footer/footer.vue';
  // import router from '@/router/index'
  import guestSection from './Guest.vue'
  import { HotelDetails } from '@/mixins/hotel-details.js';
  import { BidDetails } from '@/mixins/bid-details.js';
  import { Constants } from '@/config/constants.js'
  
  export default {
    
      name: 'GuestUserBookingDetail',

      mixins: [ HotelDetails, BidDetails ],
      
      components: { guestSection },

      props:['user','subdomain','countries'],

      data() {
        return {
          showModal:false,
          criteria:{},
          defaultActive: this.user===0?'step1':'step2',
          registerData: {
                  first_name: '',
                  last_name: '',
                  email: '',
                  contact_number: '',
                  code:'',
                  travel_purpose:'',                  
                  term_condition:'',
                  marketing_promotion:'0',
                  optionList: [],
                  title:'MR'
              }
          }             
      },

      mounted: function () {

          this.$nextTick(function () {

              let token = this.$store.getters.token;
              
              let guestDetail = this.$store.getters.getGuestDetail;

          })
      },
      
      computed: {
         url() {

              if(this.hotel) {

                  let website = this.hotel.website;

                  var url = 'background:url(' + Constants.filePath + '/uploads/';
                  
                  if (this.hotel !== null) url += this.hotel.primary_image;

                  //url += ') center center / cover no-repeat;';
                  url += ');';

                  return url;
              }

              return '';
          },
        
          no_of_day() {
              var numberOfDays;
              var checkin = this.criteria.check_in;
              var checkout = this.criteria.check_out;
              if(this.criteria.use_type == 1 && this.criteria.duration == 1) {
                return numberOfDays = '1 '+this.$t('message.HOUR_SMALL');
              } else if(this.criteria.use_type == 1 && this.criteria.duration > 1) {
                return numberOfDays = this.criteria.duration+' '+this.$t('message.HOURS_SMALL');
              }
              numberOfDays = moment(checkout).diff(moment(checkin), 'days');
              if(numberOfDays>1) {
                  numberOfDays = numberOfDays+' '+ this.$t('message.NIGHTS_SMALL');
              } else {
                  numberOfDays = numberOfDays+' '+ this.$t('message.NIGHT_SMALL');
              }
              return numberOfDays;
          },

          no_of_guest() {
            let totalGuest = 0, noOfGuest = '';
            let rooms  = this.criteria['rooms[]'];
            if(Array.isArray(rooms)) {
                rooms.forEach(function(value, key) {
                    totalGuest += parseInt(value);
                });
            } else {
                totalGuest = parseInt(rooms);
            }
            if (totalGuest > 1) noOfGuest = totalGuest+' '+this.$t('message.GUESTS_SMALL');
            else noOfGuest = '1 '+this.$t('message.GUEST_SMALL');
            return noOfGuest;
          },

          guest() {
              var guestText;
              var checkin = this.criteria.check_in;
              var checkout = this.criteria.check_out;
              if(this.criteria.guest == 1) {
                return guestText = this.$t('message.GUEST_SMALL');
              } else {
                return guestText = this.$t('message.GUESTS_SMALL');
              }
          },

          percentage_discount() {
            if(this.criteria.unit == 'ticket' && this.criteria.use_type == 1){
              let actual_prices = this.totalPrice(this.selectedBidRoom, this.currency, this.criteria.ticket);
              return Math.round(((this.bidDetail.bid_amount*100) / actual_prices)-100);
            }else{
              return Math.round(((this.bidDetail.bid_amount*100) / this.bidDetail.actual_price)-100);
            }
          },
          day_check_out_date() {
               var checkOutDay;
              let day_check_out_date = 0;
              if(this.criteria.hasOwnProperty('check_in')){
                  let check_in  = this.criteria['check_in'];
                  let mode = this.criteria['use_type'];
                  let duration = this.criteria['duration'];
                  let check_in_time = this.criteria['check_in_time'];
                  let check_in_datetime = moment(check_in + " " + check_in_time);
                  if(mode == 1){
                    checkOutDay = moment(check_in_datetime).add(duration, 'hours').format('DD.MM.YYYY');
                  }
                  return checkOutDay;
              }
              return '';
          },
          day_check_out_time() {
              var checkOutTime;
              if(this.criteria.hasOwnProperty('check_in_time') && this.criteria.hasOwnProperty('duration')){
                  let check_in  = this.criteria['check_in'];
                  let mode = this.criteria['use_type'];
                  let duration = this.criteria['duration'];
                  let check_in_time = this.criteria['check_in_time'];
                  if(mode == 1){
                    checkOutTime = moment(check_in_time, 'HH:mm').add(duration, 'hours').format('HH:mm');;
                  }
                  return checkOutTime;
              }
              return '';
          }
      },
      
      created() {

          this.$store.commit('setActivePage', 'hotel');

          this.criteria = this.$store.getters.getSearchCriteria;

          this.$store.commit('setToken', null);
          if(Object.keys(this.bidDetail).length < 1) window.location.href = '/';

          this.$nextTick(function () {
              $("#currenyDropdown").addClass('disabledbutton' );
              $("#unitConversion").addClass('disabledbutton' );
              $("#languageConversion").addClass('disabledbutton' );
          })

      },

        no_of_ticket() {
              let totalTicket = 0, noOfTicket = '' ;
              if(this.criteria.hasOwnProperty('ticket')){
                  totalTicket  = this.criteria['ticket'];
                  
                  if (totalTicket > 1) noOfTicket = totalTicket+' '+this.$t('message.TICKETS');
                  else noOfTicket = '1 '+this.$t('message.TICKETS');
                  return noOfTicket;
              }
              return '';
          },

      methods: {

        login(){
          this.showModal = true;
        },

        onClose() {
            this.showModal = false;
        },

        disabledDropDown(){
            $("#currenyDropdown").attr( "disabled", true );
            $("#unitConversion").prop( "disabled", true );
            $("#languageConversion").prop( "disabled", true );
        },
        
        onContinue(registerData){
            registerData['contact_number'] = registerData['contact_number'].replace(/\s+/g, "");

            this.$store.commit('setGuestDetail', registerData);

            this.checkoutPage( registerData );
        },
        
        checkoutPage(registerData) {
            
            this.bidDetail['guestDetail'] = registerData;
            
            //call search submit in bid-details mixin
            this.searchbidSubmit( this.bidDetail, 'after-enter-guest-detail' );
        },

        onBack(){
          this.defaultActive = 'step1';
        },

        onVerify(){
          this.checkoutPage();
        },

        onResendCodeToEmail(){
          this.registerData.type = 'email';
              this.$axios.post('/otp-send', this.registerData)
                .then((response) => {

                });
        },

        onResendCodeToMobile(){
            this.registerData.flag=1;
            this.registerData.type='mobile';
            this.$axios.post('/otp-send', this.registerData)
              .then((response) => {
            });
        },
        totalPrice(rooms, currency, days) {
              let totalPrice = 0;
              Object.keys(rooms).forEach(function(key) {
                  totalPrice += (rooms[key].avgRateUserCurrency) * (days);
                  // totalPrice += (rooms[key].count) * (rooms[key].avgRateUserCurrency) * (days);
              });
              let amount = parseFloat(totalPrice).toFixed(2);
              
              return amount;
          }

      },

      filters:{

          dateFormate: function (date) {
              return moment(date).format("DD MMM YYYY");
          },
          
          priceFormat(amount,currency){
            var parts = amount.toString().split(".");
              if(currency == 'IDR'){
                  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  return parts.join(".");
              } else {
                  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "'");
                  return parts.join(".");
              }
            return amount;
          },

          totalPriceFormat(rooms, currency, days) {
              let totalPrice = 0;
              Object.keys(rooms).forEach(function(key) {
                  totalPrice += (rooms[key].count) * (rooms[key].avgRateUserCurrency) * (days);
              });
              let amount = parseFloat(totalPrice).toFixed(2);
              var parts = amount.toString().split(".");
              if(currency == 'IDR'){
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
              } else {
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "'");
                return parts.join(".");
              }
              return amount;
          },

          totalTicketPriceFormat(rooms, currency, days) {
              let totalPrice = 0;
                Object.keys(rooms).forEach(function(key) {
                  totalPrice += (rooms[key].avgRateUserCurrency) * (days);
              });
              
              let amount = parseFloat(totalPrice).toFixed(2);
              var parts = amount.toString().split(".");
              if(currency == 'IDR'){
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
              } else {
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "'");
                return parts.join(".");
              }
              return amount;
          },

          roomDetail(rooms) {
              let counter = 0, temp = '';
              Object.keys(rooms).forEach(function(key) {
                  if(counter > 0) temp += ', ';
                  counter++;
                  temp += rooms[key].count+'x '+ rooms[key].title;
              });
              return temp;
          },

          roomCount(rooms) {
              let counter = 0, temp = 0;
              Object.keys(rooms).forEach(function(key) {
                  // if(counter > 0) temp += ', ';
                  counter++;
                  temp += rooms[key].count+ ' Room';
              });
              return temp;
          },
           roomTitle(rooms) {
              let counter = 0, temp = '';
              Object.keys(rooms).forEach(function(key) {
                  if(counter > 0) temp += ', ';
                  counter++;
                  temp += rooms[key].title;
              });
              return temp;
          }

      }

  }
