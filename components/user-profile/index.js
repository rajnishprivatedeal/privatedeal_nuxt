import Review from '@/components/user-profile/partials/Review.vue'
import Coupon from '@/components/user-profile/partials/Coupon.vue'
import CancelDayUse from '@/components/user-profile/partials/Cancelled.vue'
import CreditCard from '@/components/user-profile/partials/CreditCard.vue'
// import Profile from '@/components/user-profile/partials/Home'
import Profile from '@/components/user-profile/partials/Profile'
import Booking from '@/components/user-profile/partials/Booking'
import { Constants } from '@/config/constants.js'

export default {
    
    name: 'UserProfile',

    data() { 
    	return {
            defaultActive: 'profile',
            domain:'',
    	}  
    },

    components: {
        Profile,
        Booking,
        Review,
        Booking,
        Coupon,
        CancelDayUse,
        CreditCard
    },

    created() {

        this.$store.commit('setActivePage', 'hotel');

        this.$store.commit('setLanguage', this.language);
        
        this.domain = Constants.hostName;

        
        
    },

    mounted: function () {
        this.$nextTick(function () {
            var vm =this;
            this.$axios.get('/profile')
            .then((response) => {
                this.$store.commit('setUserDetail' , response.data.data);
                setTimeout(() => { console.log("test!"); }, 2000);
            })
        })
    },

    computed: {
        url() {
            var url = 'background:url('
            
            if (this.data.picture) {
                url += Constants.filePath + '/uploads/' + this.data.picture
            }else{
                url += Constants.filePath + '/images/user.png'
            }
            
            url += ') 50%/cover;"'

            return url
        },
        
        isGroup() {
            return this.$store.getters.domain;
        },

        data(){
            return this.$store.getters.userDetail;
        },

        language() {
            return this.$store.getters.getLanguage;
        },
    },

    // watch:{
        beforeRouteEnter (to, from, next) {
            next(vm => {
                let token = vm.$store.getters.token;
                if(token == null || token == undefined){
                    vm.$router.push({ name: 'welcome'})
                } else {
                    next();
                }
            })
          },
          
    // },

    methods: {
    	
    },    

    filters:{ 

        moment: function (date) {
            return moment(date).format('DD/MM/YYYY');
        },

        datemonth: function (date) {
            return moment(date).format('MMMM YYYY');
        },
    }, 

}