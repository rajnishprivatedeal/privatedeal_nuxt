/* ============
 * Search Component
 * ============
 *
 * todo: add documentation here!
 */

import logout from '@/components/Logout.vue'
import dateRangePicker from '@/components/DateRangePicker.vue'
import datePicker from '@/components/Datepicker.vue'
import router from '@/router/index'
import { mixin as onClickOutside } from 'vue-on-click-outside'
import { HotelDetails } from '@/mixins/hotel-details.js';
import { Constants } from '@/config/constants.js'
import { serverBus } from '../../main';

export default {

    name: 'groupSearch',

    mixins: [onClickOutside, HotelDetails],

    components: {
        logout,
        dateRangePicker,
        datePicker
    },

    props: ['criteria', 'classDisable', 'subdomain', 'place', 'placeId', 'homeIcon'],

    data: () => ({
        showRoom: true,
        focused: false,
        disableSearch: '',
        status: 'new',
        showPopover: false,
        minStayFlag: false,
        isSelected: '',
        roomSelection: false,
        checkin: this.criteria ? this.criteria.checkin : '',
        checkout: this.criteria ? this.criteria.checkout : '',
        rooms: [],
        guest: this.criteria ? this.criteria.guest : 2,
        room: this.criteria ? this.criteria.room : 1,
        guestText: null,
        roomText: null,
        count: 1,
        useType: this.criteria ? this.criteria.useType : true,
        serviceApplied: 2,
        minStay: 1,
        tax_width: 765,
        hoursList: [],
        check_in_time: '',
        duration:1,
        min_hours:1,
        max_hours:6,
        sysMin:0,
        sysMax:0,
        check_out_time:'',
        flexible:0,
        hoursApiRes:[],
        search_url: '/night-use-search-hotel',
        availableDateArray:'',

        white_label_url: '',
        hotelName: null,
        hotelUrl: null,
        showHotellist: false,
        type: 'hotel_id',
        id: '',
        name: '',
        optionGroups: [],
        availableHotel:true,
        totalHotel:0,
        minStayFlag:false,
        hotelError:false,
        minStayError:false,
        checkInTimeError:false,
        durationError:false,
        guestCameFrom:0,
        serviceData:[],
        serviceId:0,
        unit:"hourly",
        ticket:2,
        serviceCount:0,
        country_id:''
    }),

    computed: {


        serviceType (){
            return this.$store.getters.getServiceType;
        }

    },


    mounted () {
        this.$nextTick(function () {
            setTimeout(function () {
                this.tax_width = $(".date-change").innerWidth() + $(".search-button").innerWidth();
                $(".hotel-search-detail").width(this.tax_width);
            }, 100);
            if( this.groupId ) {
                this.hotelName = this.hotel.hotel_name;
                this.name = this.hotel.id;
                this.id = this.hotel.id;
                this.country_id = this.hotel.country_id;
            }

            if (this.domainDetails !== null) {
                if(this.hotel) {
                    this.serviceApplied = this.hotel.service_applied;
                    if (this.serviceApplied == '0') {
                        this.minStay = 0;
                        this.useType = false;
                    } else if (this.serviceApplied == '1' && !this.getUrlParameter('check_in') && !this.getUrlParameter('check_out')) {
                        this.minStay = this.domainDetails.minimum_stay;
                        this.useType = true;
                    } else if (this.serviceApplied == '2' && !this.getUrlParameter('check_in') && !this.getUrlParameter('check_out')) {
                        this.minStay = this.domainDetails.minimum_stay;
                    }
                } else {
                    this.serviceApplied = this.domainDetails.service_applied;
                }
                
                if (this.getUrlParameter('use_type')) this.useType = this.getUrlParameter('use_type') == 1 ? false : true;

                if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out')) {
                    this.checkin =  this.getUrlParameter('check_in');
                    this.checkout = this.getUrlParameter('check_out');
                    $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                    $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                } else if (this.useType) {
                    this.getHotelAvailability('white_brand', true);
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().add(this.minStay, "days").format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().add(this.minStay, "days").format('YYYY-MM-DD');
                    // this.minStay = this.domainDetails.minimum_stay;
                } else {
                    this.minStay = 0;
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().format('YYYY-MM-DD');                    
                    this.getHotelAvailability('white_brand', false);
                    this.getHotelCheckInHours('white_brand');
                    if (this.getUrlParameter('check_in')) this.checkin =  this.getUrlParameter('check_in');
                }
            } else {
                $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                $("#checkout").val(moment().add(1, "days").format('DD.MM.YYYY'));
                this.checkin = moment().format('YYYY-MM-DD');
                this.checkout = moment().add(1, "days").format('YYYY-MM-DD');
            }

            if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out')) {
                this.checkin =  this.getUrlParameter('check_in');
                this.checkout = this.getUrlParameter('check_out');
                $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
            }
            
            if (this.getUrlParameter('use_type')) {
                this.useType = this.getUrlParameter('use_type') == 1 ? false : true;
                this.getHotelAvailability('white_brand',  this.useType);
                this.getHotelCheckInHours('white_brand');
            }
            
            if (this.getUrlParameter('from')) {
                this.guestCameFrom = this.getUrlParameter('from');
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('check_in_time')) {
                this.check_in_time = this.getUrlParameter('check_in_time') ? this.getUrlParameter('check_in_time') : (this.hoursList[0] ? this.hoursList[0] : '00:00');
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('unit')) {
                this.unit = this.getUrlParameter('unit') ? this.getUrlParameter('unit') : 'hourly';
            }
            
            if (this.getUrlParameterArray('rooms[]').length) {
                let roomParamArr = this.getUrlParameterArray('rooms[]');
                let roomArr =[];
                $.each(roomParamArr, function (value, key) {
                    roomArr.push({ guest: key });
                });
                this.rooms = roomArr;
                //count guests
                this.guestCount();
            } else {
                if(this.domainDetails.number_of_person > 1)
                    this.rooms.push({ guest: 2 });
                else
                    this.rooms.push({ guest: 1 });
                //this.guestCount();
            }
            
            // Get group hotels listing
            
              this.getGroupHotel();
            
            serverBus.$on('change-group-language' , (str) => {
                this.getGroupHotel();
                this.guestCount();
            });

        });
    },
    
    filters:{
        
        nameFilter: function (name) {
           if (name.length > 16) {
              return _.trim(name.substring(0, 16)) + '...';
            } else {
              return name;
            }
        },
    },

    methods: {
        
        getGroupHotel() {

            let apiURL = this.language ? '/group-hotels-list/' + this.language : '/group-hotels-list'; 

            let params = {};
            
            if(this.useType) params['use_type'] = 1
            else params['use_type'] = 0
            if(this.groupId) params['group_id'] = this.groupId;

            else params['group_id'] = this.domainDetails.id;
            
            this.axios.post(apiURL, params).then(response => {
                // Setting up group hotel options
                this.optionGroups = response.data.data;
                // this.totalHotel = response.data.count_hotel;
            }).catch(error => {
                console.log(error);
            });
        },

        minimumStay(){
            let vm = this;
            this.axios.get('/get-min-stay?check_in='+moment(this.checkin).format('YYYY-MM-DD')+'&hotel_id='+this.name)
               .then((response) => {
                vm.$nextTick(function () {
                    vm.minStay = response.data.data.minimum_stay;
                });
            }); 
        },

        minimumStayForGroup(){
            let vm = this;
            this.axios.get('/get-min-stay-for-group?check_in='+moment(this.checkin).format('YYYY-MM-DD')+'&group_id='+this.domainDetails.id)
               .then((response) => {
                vm.$nextTick(function () {
                    vm.minStay = response.data.data;
                });
            }); 
        },
        
        selectedHotel(hotelId, hotelName, hotelUrl, serviceApplied,country_id) {
            $('[id^="selected_"]').removeClass('selected');
            this.closeHotelList();
            this.name = hotelId;
            $('#selected_'+hotelId).addClass('selected');
            this.hotelName = hotelName;
            this.hotelUrl = hotelUrl;
            this.id = hotelId;
            this.country_id = country_id;
            this.serviceApplied = serviceApplied;
            if (serviceApplied == '0') {
                this.useType = false;
            } else if (serviceApplied == '1') {
                this.minStay = this.domainDetails.minimum_stay;
                this.useType = true;
            }
            this.getHotel();
            this.getHotelCheckInHours('white_brand');
            if(this.name && this.availableHotel) {
                this.minimumStay();
            } else{
                this.minimumStayForGroup();
            }
            this.hotelError = false;
        },

        getHotel () {
            
            var protocol = location.protocol == 'http:' ? 'http://' : 'https://';

            this.white_label_url =  Constants.httpProtocol + this.hotelUrl + '.' + Constants.hostName;
            //this.white_label_url =  'http://localhost:8080';

            if(this.name && this.useType) {
                this.getHotelAvailability('white_brand', true);
                this.getHotelCheckInHours('white_brand');
            } else if(this.name) {
                this.getHotelCheckInHours('white_brand');
                this.getHotelAvailability('white_brand', false);
            }
        },

        openHotelList() { 
            this.showHotellist = true;
        },

        closeHotelList() {
            this.showHotellist = false;
        },

        getUrlParameterArray(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1));
            var array =[]
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    array.push(sParameterName[1]);
                }
            }
            return array;
        },
        
        getUrlParameter (name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        
        // Get hotel check hours
        getHotelCheckInHours_old (type) {
            if(!this.name) {
                return false;
            }
            let request = {
                hotel_id: this.id,
                check_in: moment(this.checkin).format('YYYY-MM-DD'),
                search_type: type
            };
            
            this.check_in_time = '';
            this.axios.post('/get-hotel-checkin-hours', request)
            .then((response) => {
                this.hoursList = response.data.data ? response.data.data.check_in : null;
                this.hoursApiRes = response.data.data;
                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = response.data.data.day_use_flexible;
                    this.check_out_time = response.data.data.day_use_checkout;
                     this.minMaxCalculation(2);
                }else if (this.check_in_time == '') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = response.data.data.day_use_flexible;
                    this.check_out_time = response.data.data.day_use_checkout;
                     this.minMaxCalculation(1);
                }
            }).catch((error) => {
                console.log(error);
                this.hoursList = [];
            });
        },

        getHotelCheckInHours (type) {
             if(!this.name) {
                return false;
            };
            let request = {
                hotel_id: this.id,
                country_id: this.country_id,
                check_in: moment(this.checkin).format('YYYY-MM-DD'),
                search_type: type
            };
            if(! this.useType){
            this.serviceId = this.serviceType;
            this.$store.commit('setServiceType', this.serviceId)
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
            let dayUseServiceCheckinHours = this.$store.getters.dayUseServiceGroupCheckinHours;
            // console.log(dayUseServiceCheckinHours[vueKey])
            if(Object.keys(dayUseServiceCheckinHours).length == 0 || ! dayUseServiceCheckinHours[vueKey]){
            // this.unit = 'hourly';
            
            // console.log(this.serviceType)
           
            this.check_in_time = '';
            this.axios.post('/get-day-use-service-checkin-hours', request)
            // this.axios.post('/get-hotel-checkin-hours', request)
            .then((response) => {
                this.serviceData = response.data.data ? response.data.data : null;
                let index = _.findIndex(this.serviceData, { 'serviceId': "0" });
                if(this.serviceData.length ==1 || index==0){
                    this.serviceCount = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length>1){
                indexes = index;
                }
                this.removeEmptyRoom(indexes,index);
                if(index == -1){
                    index = 0;
                }
               
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                // console.log(this.hoursList)
               var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
                let setDayKey = {
                    [vueKey] : response.data.data ? response.data.data : null
                };
                console.log(setDayKey)
                this.$store.commit('setDayUseServiceGroupCheckinHours', setDayKey);

                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(1);
                }
            }).catch((error) => {
                this.hoursList = [];
            })
        }else{
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
            this.serviceData = dayUseServiceCheckinHours[vueKey];
                let index = _.findIndex(this.serviceData, { 'serviceId': "0" });

                if(this.serviceData.length ==1 || index==0){
                    this.serviceCount = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length > 1){
                var indexes = index;
                }
                if(index == -1){
                    index = 0;
                }

                this.removeEmptyRoom(indexes,index);
               
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                console.log(this.hoursApiRes);
                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(1);
                }
        }
    }
        },

        minMaxCalculation(from) {
            var min_max_rooms = this.hoursApiRes.min_max_rooms;
            if(Object.keys(min_max_rooms).length == 0 && !this.useType) {
                this.durationError = true;
                this.disableSearch =  this.useType ? '' : 'disabledSearch';
                return false;
            }
            let min = 0;
            let max = 0;
            let count = 0;
            for(const room in min_max_rooms) {
                if(count == 0) {
                    min = min_max_rooms[room].min
                    max = min_max_rooms[room].max
                     count++;
                } else {
                    count++;
                    if(min_max_rooms[room].min < min) {
                        min = min_max_rooms[room].min;
                    }
                    if(min_max_rooms[room].max > max){
                        max = min_max_rooms[room].max;
                    }
                }
            }

            if(this.flexible == 1 && this.hoursApiRes.room_closed_next_day.length > 0) {
                let room_closed_next_day = this.hoursApiRes.room_closed_next_day;
                count = 0;
                let flag = false;
                let checkout_hour = 24;
                let checkin_hour = this.check_in_time.split(':');
                let totalHours = checkout_hour - checkin_hour[0];
                if (totalHours < max ) {
                    for(const room in min_max_rooms) {
                        for( const val in room_closed_next_day) {
                            if(room_closed_next_day[val] == room) {
                                max = totalHours
                                flag = true;
                                break;
                            }
                        }
                        if(flag) continue;

                        if(count == 0) {
                            min = min_max_rooms[room].min
                            max = min_max_rooms[room].max
                            count++;
                        } else {
                            count++;
                            if(min_max_rooms[room].min < min) {
                                min = min_max_rooms[room].min;
                            }
                            if(min_max_rooms[room].max > max){
                                max = min_max_rooms[room].max;
                            }
                        }
                    }
                }
            }
            this.sysMin = min
            this.sysMax = max
            this.getHours(min,max,from);
        },

        getHours(min_hours,max_hours,from) {
            this.checkInTimeError = false;
            this.disableSearch = '';
            if(this.flexible == 1) {
                if( from == 2) {
                    this.min_hours = min_hours;
                    this.duration = this.duration;
                    this.max_hours = max_hours;
                    return;
                } else if (from != 3) {
                this.minMaxCalculation(3)
                } else {
                    this.min_hours = min_hours;
                    this.duration = min_hours;
                    this.max_hours = max_hours;
                    return ;
                }
            } else {
                let checkout_hour = this.check_out_time.split(':');
                let checkin_hour = this.check_in_time.split(':');
                let totalHours = checkout_hour[0] - checkin_hour[0];
                if(totalHours < max_hours && totalHours >= this.sysMin ) {
                    if(from == 2) {
                        this.max_hours = max_hours = totalHours
                        this.duration = this.duration
                        this.min_hours = min_hours = this.sysMin
                        return;
                    } else {
                        this.min_hours = min_hours = this.sysMin
                        this.max_hours = max_hours = this.sysMax
                    }   
                } else if(totalHours > max_hours && totalHours >= this.sysMin ) {
                    if(from == 2) {
                        this.max_hours = max_hours = this.sysMax
                        this.duration = this.duration
                        this.min_hours = min_hours = this.sysMin
                        return;
                    } else {
                        this.min_hours = min_hours = this.sysMin
                        this.max_hours = max_hours = this.sysMax
                    }   
                }
                if(totalHours >= max_hours && max_hours != 0) {
                   this.min_hours = min_hours;
                   this.duration = min_hours;
                   this.max_hours = max_hours;
                   return false;

                } else if(totalHours < max_hours && totalHours >= min_hours)  {
                   this.min_hours = min_hours;
                   this.duration = from == 2 ? this.duration : min_hours;
                   this.max_hours = totalHours;
                   return false;
                } else if(totalHours < max_hours && totalHours < min_hours)  {
                    this.min_hours = 0;
                    this.duration = min_hours;
                    this.max_hours = 0;
                    this.durationError = true;
                    this.disableSearch = this.useType ? '' : 'disabledSearch';
                } else {
                    this.min_hours = 0;
                    this.duration = min_hours;
                    this.max_hours = 0;
                    this.durationError = true;
                    this.disableSearch =  this.useType ? '' : 'disabledSearch';
                    return false;
                } 
            }
        },

        // Get hotel availability
        getHotelAvailability (type, use_type) {

            if(!this.name) {
                this.availableDateArray = [];
                return false;
            }

            let request = {
                hotel_id: this.name, //11,
                search_type: type, //2,
                use_type: use_type
            };
            this.serviceId=this.serviceType;
            this.axios.post('/get-hotel-availability', request)
            .then((response) => {

                this.availableDateArray = response.data.data ? response.data.data : null;

                if ( this.availableDateArray.message == undefined ) this.validateCheckInDate();

            }).catch((error) => console.log(error));
        },

        availableHotelInSelectedRange() {
            $('[id^="selected_"]').removeClass('disableHotel');
            var vm = this;
            vm.minStayError = false;

            this.axios.post('/get-hotel-in-selectced-range',{group_id:this.domainDetails.id, check_in:moment(this.checkin).format('YYYY-MM-DD'),check_out:moment(this.checkout).format('YYYY-MM-DD')})
            .then((response) => {
                if(response.data) {
                    vm.availableHotel = false;
                    for(var i=0;i<response.data.data.length;i++) {
                        var id = "selected_"+response.data.data[i];
                        $("#"+id).addClass('disableHotel');
                    }
                    if(vm.totalHotel == i && i != 0) {
                        vm.hotelError = false;
                        $("#minStayError").text( _.replace(this.$t('message.MINIMUM_STAY_ERROR'), '(min_stay)', this.minStay));
                        vm.minStayError = true;
                    }
                }
            })
            .catch((error) => {
            })
        },

        open () {
            this.showPopover = true;
        },

        close () {
            this.showPopover = false;
        },

        disabledBtnSearch () {
            this.disableSearch = 'disabledSearch';
            this.id = '';
            this.type = '';
        },

        getList () {
            this.axios.get('/auto-suggestion?text=' + this.name)
                .then((response) => {
                    this.hasItems = true;
                    this.items = response.data;
                })
                .catch((error) => {
                    this.hasItems = false;
                })
        },

        onTopLocationClick (place, placeId) {
            this.disableSearch = '';
            this.name = place;
            this.hasItems = false;
            this.type = 'city_id';
            this.id = placeId;
        },

        onToggle () {
            this.useType = this.useType == 0 ? 1 : 0;
        },

        dateChanged () {
            this.$nextTick(function () {
                this.name = '';
                this.hotelName = ''; 
                this.optionGroups = ''; 
                // this.onToggle();
                this.getGroupHotel();
                this.getHotelAvailability('white_brand', this.useType);

                if (!this.useType) {
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().format('YYYY-MM-DD');
                } else if (this.useType) {
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().add(1, "days").format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().add(1, "days").format('YYYY-MM-DD');
                }
                this.tax_width = $(".date-change").innerWidth() + $(".search-button").innerWidth();
                $(".hotel-search-detail").width(this.tax_width);
            });
        },

        selectedItem (item) {
            this.disableSearch = '';
            this.isSelected = item.name;
            this.hasItems = false;
            this.name = item.name;
            this.type = item.type;
            this.id = item.id;
        },

      validateCheckInDate () {
            var i = 0;
            var closedDate = 0;
            var validateDate = false;
            /*I==365 added to validate date till 1 year...to avoid loop run infinitely*/
            while (! validateDate || i == 365) {
                var isAvailable = moment(this.checkin).add(i, "days").format('YYYY-MM-DD');
                if (this.availableDateArray[isAvailable] == undefined) {
                    closedDate = i+1;
                    i++;
                    continue;
                } else {
                    if (!this.useType) {
                        $("#checkin").val(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                        $("#checkout").val(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                        if (this.getUrlParameter('check_in') && this.getUrlParameter('use_type') == 1 ) {
                            this.checkin =  this.getUrlParameter('check_in');
                            $("#checkin").val(moment(this.checkin).format('DD.MM.YYYY'));
                            this.checkout =  this.getUrlParameter('check_out');
                            $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                        } else {
                            this.checkin = moment().add(closedDate, "days").format('YYYY-MM-DD');
                            this.checkout = moment().add(closedDate, "days").format('YYYY-MM-DD');
                        }
                        this.getHotelCheckInHours('white_brand');
                        if (this.getUrlParameter('check_in_time')) {
                            this.check_in_time = this.getUrlParameter('check_in_time') ;
                        }
                        validateDate = true;
                    } else if (this.useType) {
                        if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out') && this.getUrlParameter('use_type') == 2) {
                            this.checkin =  this.getUrlParameter('check_in');
                            $("#checkin").val(moment(this.checkin).format('DD.MM.YYYY'));
                        } else if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out') && this.getUrlParameter('use_type') == 2) {
                            this.checkin =  this.getUrlParameter('check_in');
                            $("#checkin").val(moment(this.checkin).format('DD.MM.YYYY'));
                             this.checkout =  this.getUrlParameter('check_out');
                            $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                        } else {
                            $("#checkin").val(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                            this.checkin = moment().add(closedDate, "days").format('YYYY-MM-DD');
                            var vm = this;setTimeout(function(){vm.minimumStay() }, 180);
                            //this.minimumStayValidatingClosedDates(closedDate) ;
                            $("#checkout").val(moment().add(closedDate + this.minStay, "days").format('DD.MM.YYYY'));
                            this.checkout = moment().add(closedDate + this.minStay, "days").format('YYYY-MM-DD');
                        }
                    }
                    i++;

                }               
                if( i >= this.minStay && this.useType ) {
                    validateDate = true;
                }
            }
        },

        addRoom () {
            this.rooms.push({ guest: 1 });
            this.guestCount();
        },

        removeRoom (index) {
            this.rooms.splice(index, 1);
            this.guestCount();
        },

        onSelected (date, type) {
            $("#date_range").text('');
            this.disableSearch = '';

            if (date) {
                if (!this.useType) {
                    if (type === 'checkin') {
                        this.checkin = moment(date).format('YYYY-MM-DD');
                        this.checkout = moment(date).format('YYYY-MM-DD');
                        $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                        $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                    } else if (type === 'checkout') {
                        this.checkin = moment(date).format('YYYY-MM-DD');
                        this.checkout = moment(date).format('YYYY-MM-DD');
                        $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                        $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                    }
                    this.getHotelCheckInHours('white_brand');
                } else if (this.useType) {
                    if (type == 'checkin') {
                        this.checkin = moment(date).format('YYYY-MM-DD');

                        if(this.name && this.availableHotel) this.minimumStay();
                        else this.minimumStayForGroup();

                        if (date >= new Date(this.checkout)) {
                            if(this.name && this.availableHotel) this.minimumStay();
                            else this.minimumStayForGroup();
                            
                            this.checkout = moment(date).add(this.minStay, "days").format('YYYY-MM-DD');
                            $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                            $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                        } else {
                            this.checkin = moment(date).format('YYYY-MM-DD');
                            $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                        }
                    } else if (type == 'checkout') {
                        this.checkout = moment(date).format('YYYY-MM-DD');
                        this.availableHotelInSelectedRange();
                        $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                    }
                } else {
                    $("#checkin-group").val(this.checkin);
                    $("#checkout").val(this.checkout);
                }
            }
        },

        input (date) {
            console.log('input--' + date);
        },

        guestCount () {
            this.showRoom = false;
            var totalGuest = 0;
            var totalRoom = 0;
            $.each(this.rooms, function (value, key) {
                totalGuest += parseInt(key.guest);
                totalRoom += 1;
            });
            this.guest = totalGuest;
            this.room = totalRoom;
            if (totalGuest > 1) {
                this.guestText = this.$t('message.GUESTS_SMALL');
            } else {
                this.guestText = this.$t('message.GUEST_SMALL');
            }
            if (totalRoom > 1) {
                this.roomText = this.$t('message.ROOMS_SMALL');
            } else {
                this.roomText = this.$t('message.ROOM_SMALL');
            }
        },
        
        // Hotel search handler
        hotelSearchHandler () {
            
            if (this.name == '') {
                this.hotelError = true; return false;
                if(this.useType) this.availableHotelInSelectedRange();
            } else if (!this.useType && (this.check_in_time == '' || this.check_in_time == '00:00')) {
                this.checkInTimeError = true; return false;
            }
            
            let checkin = $('[name="checkin"]').val(),
                checkout = $('[name="checkout"]').val(),
                mode = ((this.serviceApplied == 0) ? 1 : ((this.serviceApplied == 1) ? 2 : ($('[name="mode"]').is(':checked') ? 2 : 1)));
            
            // let route = (mode == 2) ? 'night-use-search' : 'day-use-search';
            
            let roomArr  = [];
            $.each(this.rooms, function (value, key) {
                roomArr.push(key.guest);
            });
            
            /*Mouse flow code*/
                window._mfq = window._mfq || [];
                window._mfq.push(['formSubmitAttempt', '#searchForm']);

            let params = {
                "use_type":  mode,
                "check_in":  moment(this.checkin).format('YYYY-MM-DD'),
                "check_out": (mode==1) ? moment(this.checkin).format('YYYY-MM-DD') : moment(this.checkout).format('YYYY-MM-DD'),
                "rooms[]": roomArr,
            };
            
            if(mode == 1) params['check_in_time'] = this.check_in_time;
            params['from'] = this.guestCameFrom;
            if (mode == 1) params['duration'] = this.duration==0?1:this.duration;
            // if (mode == 1 && (this.unit=='hourly'|| this.serviceId == 0)) params['duration'] = this.duration==0?1:this.duration;
            if (mode == 1 && this.unit=='ticket') params['ticket'] = this.ticket;
            if (mode == 1) params['service_id'] = this.serviceId;
            if(mode==1) this.$store.commit('setServiceType', this.serviceId);    
            params['unit'] = this.unit;
            if(this.activepage == 'home') {
                params['group'] = this.domainDetails.id;
            } else {
                params['group'] = this.groupId;
            }
            
            let route = (mode == 2) ? this.white_label_url+'/night-use-search-hotel' : this.white_label_url+'/day-use-search-hotel';
            
            params = $.param( params );

            window.location.href = route + '?' + params;
            
            //router.push({name: route, query: params});
                
        },
        
        onDateRangeChanged (picker, type) {
            if (picker) {
                if (!this.useType) {
                    if (type === 'checkin') {
                        this.checkin = picker.startDate.format('DD.MM.YYYY');
                        this.checkout = picker.startDate.format('DD.MM.YYYY');
                        $("#checkin-group").val(this.checkin);
                        $("#checkout").val(this.checkout);
                    } else if (type === 'checkout') {
                        this.checkin = picker.endDate.format('DD.MM.YYYY');
                        this.checkout = picker.endDate.format('DD.MM.YYYY');
                        $("#checkin-group").val(this.checkin);
                        $("#checkout").val(this.checkout);
                    }
                } else if (this.useType) {
                    if (type == 'checkin') {
                        if (picker.endDate.diff(picker.startDate, 'days') < 1) {
                            this.checkin = picker.startDate.format('DD.MM.YYYY');
                            this.checkout = picker.endDate.add(1, "days").format('DD.MM.YYYY');
                            $("#checkin-group").val(this.checkin);
                            $("#checkout").val(this.checkout);
                        } else {
                            this.checkin = picker.startDate.format('DD.MM.YYYY');
                            this.checkout = picker.endDate.format('DD.MM.YYYY');
                            $("#checkin-group").val(this.checkin);
                            $("#checkout").val(this.checkout);
                        }
                    } else if (type == 'checkout') {
                        this.checkin = picker.startDate.format('DD.MM.YYYY');
                        this.checkout = picker.endDate.format('DD.MM.YYYY');
                        $("#checkin-group").val(this.checkin);
                        $("#checkout").val(this.checkout);
                    }
                }
                else {
                    $("#checkin-group").val(this.checkin);
                    $("#checkout").val(this.checkout);
                }

            }
        },
        setServiceId(serviceId){
        let indexes = _.findIndex(this.serviceData, { 'serviceId': serviceId });
        let hoursListDatas = this.serviceData[indexes];
        this.unit = hoursListDatas ? hoursListDatas.unit : null;
        this.serviceId = serviceId;
        this.$store.commit('setServiceType', this.serviceId);  
        if(serviceId > 0){
            this.durationError = false;
            this.disableSearch =   '';
        }
      },

      setTicket(ticket){
        this.ticket = ticket;
      },

      removeEmptyRoom(indexes,index){
        var removeRoom = this.serviceData[index];
        var min_max_rooms = removeRoom.min_max_rooms;

        // if(Object.keys(min_max_rooms).length == 0 && this.useType == false) {
        //         if(! this.useType){
        //         delete this.hoursApiRes[indexes];
        //         delete this.serviceData[indexes];
        //     }
        //     }
      }

    }

}