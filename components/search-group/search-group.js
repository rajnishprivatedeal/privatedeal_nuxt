/* ============
 * Search Component
 * ============
 *
 * todo: add documentation here!
 */
 import Vue from 'vue';
import logout from '@/components/Logout.vue'
import dateRangePicker from '@/components/DateRangePicker.vue'
import datePicker from '@/components/Datepicker.vue'
// import router from '@/router/index'
import { mixin as onClickOutside } from 'vue-on-click-outside'
import { HotelDetails } from '@/mixins/hotel-details.js';
import { Constants } from '@/config/constants.js'
import { serverBus } from '../../main';

export default {

    name: 'groupSearch',

     mixins: [onClickOutside, HotelDetails],

    components: {
        logout,
        dateRangePicker,
        datePicker
    },

    props: ['criteria', 'classDisable', 'subdomain', 'place', 'placeId', 'homeIcon'],

    data() {
        return {
            showRoom: true,
            focused: false,
            disableSearch: '',
            showPopover: false,
            minStayFlag: false,
            showTimer:false,
            showDuration:false,
            showTicket:false,
            showService:false,
            isSelected: '',
            roomSelection: false,
            checkin: this.criteria ? this.criteria.checkin : '',
            checkout: this.criteria ? this.criteria.checkout : '',
            rooms: [],
            guest:  2,
            room: 1,
            guestText: null,
            roomText: null,
            count: 1,
            useType: 2,
            serviceApplied: 2,
            groupServiceApplied: 2,
            minStay: 1,
            min_stays:1,
            hoursList: [],
            check_in_time: '00:00',
            duration:1,
            min_hours:1,
            max_hours:6,
            sysMin:0,
            sysMax:0,
            check_out_time:'',
            flexible:0,
            hoursApiRes:[],
            search_url: '/night-use-search-hotel',
            availableDateArray:'',

            white_label_url: '',
            hotelName: null,
            hotelUrl: null,
            showHotellist: false,
            type: 'hotel_id',
            id: '',
            name: '',
            hasItems: false,
            cityName: true,
            items:[],
            optionGroups: [],
            availableHotel:true,
            totalHotel:0,
            minStayFlag:false,
            destinationError:false,
            destinationWrong:false,
            minStayError:false,
            checkInTimeError:false,
            serviceError : false,
            dateRangeError:false,
            maxCount:5,
            endPoint:'',
            langClass:'lang-en',
            posScroll:0,
            durationError:false,
            guestCameFrom:0,
            serviceData:[],
            serviceId:-1,
            unit:"hourly",
            ticket:this.ticket?this.ticket:1,
            serviceCount:this.serviceCount > 0 ? this.serviceCount : 0,
            country_id:'',
            serviceName:this.serviceName?this.serviceName:'',
            serviceIcon:this.serviceIcon?this.serviceIcon:'../../../static/images/services.png',
            // groupId:1,
        }
    },

    computed: {


        serviceType (){
            return this.$store.getters.getServiceType;
        }

    },

    mounted () {
        this.groupServiceApplied = parseInt(this.domainDetails.group_service_applied);
       
        if(this.groupServiceApplied == 0){
            this.useType = 1;
            this.getGroupHotel();
        }
        this.$nextTick(function () {
            this.setInitialDate(); 
            this.guestCount();
             serverBus.$on('change-room-language' , (str) => {
                if(this.useType == 1){
                this.getHotelCheckInHours('white_brand','',str);
            }
            });
            // this.posScroll = $("#app:first").offset().top;
            if( this.groupId ) {
                this.hotelName = this.hotel.hotel_name;
                this.name = this.hotel.hotel_name;
                this.id = this.hotel.id;
                this.country_id = this.hotel.country_id;
            }

            if (this.domainDetails !== null) {
                if(this.hotel) {
                    // this.serviceApplied = this.domainDetails.group_service_applied;
                    this.groupServiceApplied = this.serviceApplied = this.hotel.service_applied;
                    if (this.serviceApplied == '0') {
                        this.minStay = 0;
                        this.useType = 1;
                    } else if (this.serviceApplied == '1' && !this.getUrlParameter('check_in') && !this.getUrlParameter('check_out')) {
                        this.minStay = this.domainDetails.minimum_stay;
                        this.useType = 2;
                    } else if (this.serviceApplied == '2' && !this.getUrlParameter('check_in') && !this.getUrlParameter('check_out')) {
                        this.minStay = this.domainDetails.minimum_stay;
                    }
                } else {
                    this.groupServiceApplied = this.serviceApplied = this.domainDetails.group_service_applied;
                    // this.serviceApplied = this.domainDetails.service_applied;
                }

                this.min_stays = this.domainDetails.minimum_stay;
                if (this.getUrlParameter('use_type')) this.useType = this.getUrlParameter('use_type') == 1 ? 1 : 2;

                if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out')) {
                    this.checkin =  this.getUrlParameter('check_in');
                    this.checkout = this.getUrlParameter('check_out');
                    $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                    $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
                } else if (this.useType) {
                    this.getHotelAvailability('white_brand', true);
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().add(this.minStay, "days").format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().add(this.minStay, "days").format('YYYY-MM-DD');
                    // this.minStay = this.domainDetails.minimum_stay;
                } else {
                    this.minStay = 0;
                    $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                    $("#checkout").val(moment().format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().format('YYYY-MM-DD');                    
                    this.getHotelAvailability('white_brand', false);
                    this.getHotelCheckInHours('white_brand');
                    if (this.getUrlParameter('check_in')) this.checkin =  this.getUrlParameter('check_in');
                }
            } else {
                $("#checkin-group").val(moment().format('DD.MM.YYYY'));
                $("#checkout").val(moment().add(1, "days").format('DD.MM.YYYY'));
                this.checkin = moment().format('YYYY-MM-DD');
                this.checkout = moment().add(1, "days").format('YYYY-MM-DD');
            }

            if (this.getUrlParameter('check_in') && this.getUrlParameter('check_out')) {
                this.checkin =  this.getUrlParameter('check_in');
                this.checkout = this.getUrlParameter('check_out');
                $("#checkin-group").val(moment(this.checkin).format('DD.MM.YYYY'));
                $("#checkout").val(moment(this.checkout).format('DD.MM.YYYY'));
            }
            
            if (this.getUrlParameter('use_type')) {
                this.useType = this.getUrlParameter('use_type') == 1 ? 1 : 2;
                this.getHotelAvailability('white_brand',  this.useType);
                this.getHotelCheckInHours('white_brand');
            }
            
            if (this.getUrlParameter('from')) {
                this.guestCameFrom = this.getUrlParameter('from');
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('check_in_time')) {
                this.check_in_time = this.getUrlParameter('check_in_time') ? this.getUrlParameter('check_in_time') : (this.hoursList[0] ? this.hoursList[0] : '00:00');
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('check_out_time')) {
                this.check_out_time = this.getUrlParameter('check_out_time') ? this.getUrlParameter('check_out_time') : (this.hoursList[0] ? this.hoursList[0] : '00:00');
                // this.getHotelCheckInHours('white_brand');

            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('unit')) {
                this.unit = this.getUrlParameter('unit') ? this.getUrlParameter('unit') : 'hourly';
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('duration')) {
                this.duration = this.getUrlParameter('duration') ? this.getUrlParameter('duration') : '1';
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('ticket')) {
                this.ticket = this.getUrlParameter('ticket') ? this.getUrlParameter('ticket') : '1';
            }

            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('service_id')) {
                this.serviceId = this.getUrlParameter('service_id') ? this.getUrlParameter('service_id') : 0;
            }

             if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('service_name')) {
                this.serviceName = this.getUrlParameter('service_name') ? this.getUrlParameter('service_name') : '';
            }
            if (this.getUrlParameter('use_type') == 1 && this.getUrlParameter('service_name')) {
                this.serviceIcon = this.getUrlParameter('service_icon') ? this.getUrlParameter('service_icon') : '';
            }
            if(this.getUrlParameter('service_applied')){
                this.groupServiceApplied = this.getUrlParameter('service_applied') ? this.getUrlParameter('service_applied') : 2;
            }
            
            if (this.getUrlParameterArray('rooms[]').length) {
                let roomParamArr = this.getUrlParameterArray('rooms[]');
                let roomArr =[];
                $.each(roomParamArr, function (value, key) {
                    roomArr.push({ guest: key });
                });
                this.rooms = roomArr;
                //count guests
                this.guestCount();
            } else {
                if(this.domainDetails.number_of_person > 1)
                    this.rooms.push({ guest: 2 });
                else
                    this.rooms.push({ guest: 1 });
                //this.guestCount();
            }
            
            // Get group hotels listing
            
              this.getGroupHotel();
            
            serverBus.$on('change-group-language' , (str) => {
                this.getGroupHotel();
                this.guestCount();
            });
        });
    },

    destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    },

    created(){
        this.endPoint = Constants.httpProtocol+Constants.hostName+'/api/';
        this.getLocale();
        this.getGroupHotel();       
        window.addEventListener('scroll', this.handleScroll);
    },

    watch: {
        name: function (newKey) {
            this.destinationError = false;
            if(this.isSelected!==newKey){
                if( newKey.length > 0 ){
                    this.getList();
                } else {
                    this.cityName = true;
                    this.hasItems = true;
                    this.items = this.optionGroups.hotelWithCity;
                }
            }else{
                this.cityName = true;
                this.hasItems = true;
                this.items = this.optionGroups.hotelWithCity;
            }        
        }
    },

    filters:{
        
        nameFilter: function (name) {
           if (name.length > 16) {
              return _.trim(name.substring(0, 16)) + '...';
            } else {
              return name;
            }
        },
    },

    methods: {
        decreaseGuest(i){
            if(parseInt(this.rooms[i].guest) > 1)
                this.rooms[i].guest -= 1;
            this.guestCount();
        },

        increaseGuest(i){
            if(this.rooms[i].guest < this.maxCount)
               this.rooms[i].guest = parseInt(this.rooms[i].guest)+1;;
            this.guestCount();
        },

        disabledBtnSearch(){
            this.disableSearch = 'disabledSearch';
        },

        getLocale(){
            var url = window.location.href.split('/');
            if(url.length >= 4 && (url[3] == 'en' || url[3] == 'fr' || url[3] == 'de'  || url[3] == 'es' )){
               Vue.config.lang = url[3];
                this.langClass = 'lang-'+url[3];
            }
            else {
                Vue.config.lang = 'en' ;
                this.langClass = 'lang-en';
            }
        },

        handleScroll() {
        },

        getGroupHotel() {
            // let apiURL = this.endPoint+'auto-suggestion'; 
             let apiURL = '/auto-suggestion'; 

            this.serviceError = false;
            this.disableSearch =   '';

            let params = {};
            params['use_type'] = this.useType

           if(this.groupId) params['group_id'] = this.groupId;

            else params['group_id'] = this.domainDetails.id;

            params['lang_code'] = Vue.config.lang;
            
            if(this.useType == 1) params['check_in_time'] = this.check_in_time;

            this.$axios.post(apiURL, params).then(response => {
                // Setting up group hotel options
                this.optionGroups = response.data.data;
                // this.totalHotel = response.data.count_hotel;
            }).catch(error => {
                /*console.log(error);*/
            });
        },

        selectTime(option){
            if(this.serviceId > -1){
            this.check_in_time = option;
            this.getHours(this.min_hours,this.max_hours)
        }else{
            this.serviceError = true;
        }
            this.closeTime();
        },

        selectDuration(option){
            this.duration = option;
            this.closeDuration();
        },

        async minimumStay(){
            let vm = this;
            await this.$axios.get(this.endPoint+'get-min-stay?check_in='+moment(this.checkin).format('YYYY-MM-DD')+'&hotel_id='+this.id)
               .then((response) => {
                vm.$nextTick(function () {
                    vm.minStay = response.data.data.minimum_stay;
                });
            }); 
        },

        maxPersonAllowed(){
            let vm = this;
            this.$axios.get(this.endPoint+'get-max-person-allowed?hotel_id='+this.id)
               .then((response) => {
                vm.$nextTick(function () {
                    vm.maxCount = response.data.data;
                });
            }); 
        },


        async minimumStayForGroup(){
            let vm = this;
            await this.$axios.get(this.endPoint+'get-min-stay-for-group?check_in='+moment(this.checkin).format('YYYY-MM-DD')+'&group_id='+this.groupId)
               .then((response) => {
                vm.$nextTick(function () {
                    vm.minStay = response.data.data;
                });
            }); 
        },
        
        clearHotelName(){
            this.destinationError = false;
            this.destinationWrong = false;
            this.setInitialDate();           
            this.dateRangeError   = false;
            this.id = '';
            this.name = '';
            this.isSelected = '';
            this.hotelName = '';
            this.duration = 1;
            this.disableSearch = 'disabledSearch'; 
        },

        setInitialDate() {
            $("#check-in-date").text(moment().format('DD.MM.YYYY'));
            $("#check-out").text(moment().add(1, "days").format('DD.MM.YYYY'));
            this.checkin = moment().format('YYYY-MM-DD');
            this.checkout = moment().add(1, "days").format('YYYY-MM-DD');
        },

        selectedHotel(data,country_id) {
            this.disableSearch = '';            
            this.setInitialDate(); 
            this.closeHotelList();
            this.id = data.id;
            this.name = data.hotel_name;
            this.isSelected = data.hotel_name;
            this.hotelName = data.hotel_name;
            this.hotelUrl = data.whitebrand.hotel_url;
            this.groupServiceApplied = this.serviceApplied = data.service_applied;
            this.country_id = country_id;
            this.maxPersonAllowed();
            if (this.serviceApplied == '0') {
                this.useType = 1;
            } else if (this.serviceApplied == '1') {
                this.minStay = data.whitebrand.minimum_stay;
                this.useType = 2;
            }
            this.getHotel();
            if(this.name && this.availableHotel) {
                this.minimumStay();
            } else{
                this.minimumStayForGroup();
            }
            this.destinationError = false;
            this.destinationWrong = false;
        },

        getHotel () {
            
            var protocol = location.protocol == 'http:' ? 'http://' : 'https://';

            this.white_label_url =  Constants.httpProtocol + this.hotelUrl + '.' + Constants.hostName;
            //this.white_label_url =  'http://localhost:8080';
            if(this.name && this.useType == 2) {
                this.getHotelAvailability('white_brand');
            } else if(this.name) {
                this.getHotelCheckInHours('white_brand');
                this.getHotelAvailability('white_brand');
            }
        },

        openHotelList() { 
            this.$nextTick(function () {
                this.hasItems = true;
                this.showHotellist = true;
            });
        },

        closeHotelList() {
            this.$nextTick(function () {
                this.hasItems = false;
                this.showHotellist = false;
            });
        },

        getUrlParameterArray(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1));
            var array =[]
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    array.push(sParameterName[1]);
                }
            }
            return array;
        },
        
        getUrlParameter (name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        
        // Get hotel check hours
        getHotelCheckInHours_old (type) {
            if(!this.name) {
                return false;
            }
            let request = {
                hotel_id: this.id,
                check_in: moment(this.checkin).format('YYYY-MM-DD'),
                search_type: type
            };
            
            this.check_in_time = '00:00';
            this.$axios.post(this.endPoint+'get-hotel-checkin-hours', request)
            .then((response) => {
                this.hoursList = response.data.data ? response.data.data.check_in : null;
                this.hoursApiRes = response.data.data;
                if (this.check_in_time == '' || this.check_in_time == '00:00') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = response.data.data.day_use_flexible;
                    this.check_out_time = response.data.data.day_use_checkout;
                    this.minMaxCalculation(1);
                }
            }).catch((error) => {
                /*console.log(error);*/
                this.hoursList = [];
            });
        },

        getHotelCheckInHours_1 (type) {
             if(!this.name) {
                return false;
            };
            let request = {
                hotel_id: this.id,
                country_id: this.country_id,
                check_in: moment(this.checkin).format('YYYY-MM-DD'),
                search_type: type
            };
            if(this.useType==1){
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
            let dayUseServiceCheckinHours = this.$store.getters.dayUseServiceGroupCheckinHours;
            if(Object.keys(dayUseServiceCheckinHours).length == 0 || ! dayUseServiceCheckinHours[vueKey]){
            this.check_in_time = '00:00';
            this.$axios.post('/get-day-use-service-checkin-hours', request)
            .then((response) => {
                this.serviceData = response.data.data ? response.data.data : null;
                let index = _.findIndex(this.serviceData, { 'serviceId': "0" });
                this.$store.commit('setAcceptOnlinePayment', this.serviceData[index].accept_online_payment);
                if(this.serviceData.length ==1 || index==0){
                    this.serviceCount = 0;
                    this.serviceId = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length>1){
                indexes = index;
                }
                this.removeEmptyRoom(indexes,index);
                if(index == -1){
                    index = 0;
                }
                if(this.serviceId > 0){
                    index = _.findIndex(this.serviceData, { 'serviceId': parseInt(this.serviceId) });
                }
               
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                if(!this.serviceName)
                this.serviceName = 'Services'
                if(!this.serviceIcon && this.serviceId == -1)
                this.serviceIcon = '../../../static/images/services.png'
               var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
                let setDayKey = {
                    [vueKey] : response.data.data ? response.data.data : null
                };
                this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
                this.$store.commit('setDayUseServiceGroupCheckinHours', setDayKey);

                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'01:00';
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '' || this.check_in_time == '00:00') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = hoursListData.day_use_flexible;
                    this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'01:00';
                    this.minMaxCalculation(1);
                }
            }).catch((error) => {
                this.hoursList = [];
            })
        }else{
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type;
            this.serviceData = dayUseServiceCheckinHours[vueKey];
                let index = _.findIndex(this.serviceData, { 'serviceId': "0" });
                this.$store.commit('setAcceptOnlinePayment', this.serviceData[index].accept_online_payment);

                if(this.serviceData.length ==1 || index==0){
                    this.serviceCount = 0;
                    this.serviceId = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length > 1){
                var indexes = index;
                }
                if(index == -1){
                    index = 0;
                }

                this.removeEmptyRoom(indexes,index);
               if(this.serviceId > 0){
                    index = _.findIndex(this.serviceData, { 'serviceId': parseInt(this.serviceId) });
                }
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
                if(!this.serviceName)
                this.serviceName = 'Services'
                if(!this.serviceIcon && this.serviceId == -1)
                this.serviceIcon = '../../../static/images/services.png'
                 if (this.getUrlParameter('check_out_time')) {
                    this.check_out_time = this.getUrlParameter('check_out_time') ;
                } else if (this.check_out_time == '' || this.check_out_time == '01:00') {
                    this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'01:00';
                }
                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    // this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '' || this.check_in_time == '00:00') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '';
                    this.flexible = hoursListData.day_use_flexible;
                    // this.check_out_time = hoursListData.day_use_checkout;
                    this.minMaxCalculation(1);
                }
        }
    }
},
        getHotelCheckInHours (type,useTy,lang='') {
             if(!this.name) {
                return false;
            };
            let request = {
                hotel_id: this.id,
                country_id: this.country_id,
                check_in: moment(this.checkin).format('YYYY-MM-DD'),
                search_type: type
            };
            this.checkInTimeError = false;
            if(!lang){
                lang = Vue.config.lang;
            }
            if(this.useType==1){
            this.check_in_time = this.check_in_time?this.check_in_time:'00:00';
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type+'_'+lang;
            let dayUseServiceCheckinHours = this.$store.getters.dayUseServiceGroupCheckinHours;
            if(Object.keys(dayUseServiceCheckinHours).length == 0 || ! dayUseServiceCheckinHours[vueKey]){
            this.$axios.post('/get-day-use-service-checkin-hours/'+lang, request)
            .then((response) => {
                this.serviceData = response.data.data ? response.data.data : null;
                let index = _.findIndex(this.serviceData, { 'serviceId': "0" });
                this.$store.commit('setAcceptOnlinePayment', this.serviceData[index].accept_online_payment);
                if(this.serviceData.length ==1){
                    this.serviceCount = 0;
                    this.serviceId = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length>1){
                indexes = index;
                }
                this.removeEmptyRoom(indexes,index);
                if(index == -1){
                    index = 0;
                }
                if(this.serviceId > 0){
                    index = _.findIndex(this.serviceData, { 'serviceId': this.serviceId });
                }
               
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                if(!this.serviceName || this.serviceName === 'Services')
                this.serviceName = 'Services'
                else
                this.serviceName = hoursListData.serviceName;
                if(!this.serviceIcon && this.serviceId == -1)
                this.serviceIcon = '../../../static/images/services.png'

               var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type+'_'+lang;
                let setDayKey = {
                    [vueKey] : response.data.data ? response.data.data : null
                };
                console.log(hoursListData)

                this.$store.commit('setDayUseServiceGroupCheckinHours', setDayKey);
                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    if(this.serviceId == 0){
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.day_use_checkout?hoursListData.day_use_checkout:'00:00';
                    }else{
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'00:00';
                    }
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '' || this.check_in_time == '00:00') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
                    this.flexible = hoursListData.day_use_flexible;
                    if(this.serviceId == 0 || this.serviceId == -1){
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.day_use_checkout?hoursListData.day_use_checkout:'00:00';
                    }else{
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'00:00';
                    }
                    this.minMaxCalculation(1);
                }
                
                // this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
            }).catch((error) => {
                this.hoursList = [];
            })
        }else{
            var vueKey = this.id+'_'+this.country_id+'_'+moment(this.checkin).format('YYYYMMDD')+'_'+type+'_'+lang;
            this.serviceData = dayUseServiceCheckinHours[vueKey];
                var index = _.findIndex(this.serviceData, { 'serviceId': "0" });
                this.$store.commit('setAcceptOnlinePayment', this.serviceData[index].accept_online_payment);

                if(this.serviceData.length ==1){
                    this.serviceCount = 0;
                    this.serviceId = 0;
                }else{
                    this.serviceCount = 1;
                }
                var indexes = -1;
                if(this.serviceData.length > 1){
                var indexes = index;
                }
                if(index == -1){
                    index = 0;
                }

                this.removeEmptyRoom(indexes,index);
                if(this.serviceId > 0){
                    index = _.findIndex(this.serviceData, { 'serviceId': this.serviceId });
                }
                let hoursListData = this.serviceData[index];
                this.hoursList = hoursListData ? hoursListData.check_in : null;
                this.hoursApiRes = hoursListData;
                if(!this.serviceName || this.serviceName === 'Services')
                this.serviceName = 'Services'
                else
                // this.serviceName = hoursListData.serviceName;
                if(!this.serviceIcon && this.serviceId == -1)
                this.serviceIcon = '../../../static/images/services.png'

                
                 if (this.getUrlParameter('check_out_time')) {
                    this.check_out_time = this.getUrlParameter('check_out_time') ;
                } else if (this.check_out_time == '' || this.check_out_time == '00:00') {
                    if(this.serviceId == 0 || this.serviceId == -1){
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.day_use_checkout?hoursListData.day_use_checkout:'00:00';
                    }else{
                        this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'00:00';
                    }
                }

                if (this.getUrlParameter('check_in_time')) {
                    this.check_in_time = this.getUrlParameter('check_in_time') ;
                    this.flexible = hoursListData.day_use_flexible;
                    // this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'01:00';
                    this.minMaxCalculation(2);
                } else if (this.check_in_time == '' || this.check_in_time == '00:00') {
                    this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
                    this.flexible = hoursListData.day_use_flexible;
                    // this.check_out_time = this.check_out_time?this.check_out_time:hoursListData.check_out?hoursListData.check_out:'01:00';
                    this.minMaxCalculation(1);
                }
                // this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
        }
    }
        },

        minMaxCalculation(from) {
            var min_max_rooms = this.hoursApiRes.min_max_rooms;
            let min = 0;
            let max = 0;
            let count = 0;
            for(const room in min_max_rooms) {
                if(count == 0) {
                    min = min_max_rooms[room].min
                    max = min_max_rooms[room].max
                     count++;
                } else {
                    count++;
                    if(min_max_rooms[room].min < min) {
                        min = min_max_rooms[room].min;
                    }
                    if(min_max_rooms[room].max > max){
                        max = min_max_rooms[room].max;
                    }
                }
            }

            if(this.flexible == 1 && this.hoursApiRes.room_closed_next_day.length > 0) {
                let room_closed_next_day = this.hoursApiRes.room_closed_next_day;
                count = 0;
                let flag = false;
                let checkout_hour = 24;
                let checkin_hour = this.check_in_time.split(':');
                let totalHours = checkout_hour - checkin_hour[0];
                if (totalHours < max ) {
                    for(const room in min_max_rooms) {
                        for( const val in room_closed_next_day) {
                            if(room_closed_next_day[val] == room) {
                                max = totalHours
                                flag = true;
                                break;
                            }
                        }
                        if(flag) continue;

                        if(count == 0) {
                            min = min_max_rooms[room].min
                            max = min_max_rooms[room].max
                            count++;
                        } else {
                            count++;
                            if(min_max_rooms[room].min < min) {
                                min = min_max_rooms[room].min;
                            }
                            if(min_max_rooms[room].max > max){
                                max = min_max_rooms[room].max;
                            }
                        }
                    }
                }
            }
            this.sysMin = min
            this.sysMax = max
            this.getHours(min,max,from);
        },

         getHours(min_hours,max_hours,from) {
            this.checkInTimeError = false;
            this.disableSearch = '';
            if(this.flexible == 1) {
                if( from == 2) {
                    this.min_hours = min_hours;
                    this.duration = this.duration;
                    this.max_hours = max_hours;
                    return;
                } else if (from != 3) {
                this.minMaxCalculation(3)
                } else {
                    this.min_hours = min_hours;
                    this.duration = min_hours;
                    this.max_hours = max_hours;
                    return ;
                }

            } else {
                let checkout_hour = this.check_out_time.split(':');
                let checkin_hour = this.check_in_time.split(':');
                let totalHours = checkout_hour[0] - checkin_hour[0];
                if(totalHours >= this.sysMin) {
                    if(from == 2) {
                        this.max_hours = max_hours = this.sysMax
                        this.duration = this.duration
                        this.min_hours = min_hours = this.sysMin
                        //return;
                    } else {
                        this.min_hours = min_hours = this.sysMin
                        this.max_hours = max_hours = this.sysMax
                    }   
                }
                if(totalHours >= max_hours && max_hours != 0) {
                   this.min_hours = min_hours;
                   // this.duration = min_hours;
                   this.duration = from == 2 ? this.duration : min_hours;
                   this.max_hours = max_hours;
                   return false;
                } else if(totalHours < max_hours && totalHours >= min_hours)  {
                   this.min_hours = min_hours;
                   this.duration = from == 2 ? this.duration : min_hours;
                   this.max_hours = totalHours;
                   return false;
                } else if(totalHours < max_hours && totalHours < min_hours)  {
                    this.min_hours = 0;
                    this.duration = min_hours;
                    this.max_hours = 0;
                    this.checkInTimeError = true;
                    this.disableSearch = 'disabledSearch';
                } else {
                    this.min_hours = 0;
                    this.duration = min_hours;
                    this.max_hours = 0;
                    this.checkInTimeError = true;
                    this.disableSearch = 'disabledSearch';
                    return false;
                } 
            }
        },


        // Get hotel availability
       getHotelAvailability (type, use_type) {
        if(use_type){
            this.$store.commit('setBannerUseType', 2);
        }else{
            this.$store.commit('setBannerUseType', 1);
        }
            // this.disableSearch = 'disabledSearch';
            if(!this.name) {
                this.availableDateArray = [];
                return false;
            }

            let request = {
                hotel_id: this.id, //11,
                search_type: type, //2,
                use_type: use_type==1?0:1,
            };
            if(this.serviceId != -1){
                this.serviceId=this.serviceType;
            }

            this.$axios.post(this.endPoint+'get-hotel-availability', request)
            .then((response) => {

                this.availableDateArray = response.data.data ? response.data.data : null;
                this.disableSearch = '';
                this.serviceError = false;
                if ( this.availableDateArray.message == undefined ) this.validateCheckInDate();
                this.disableSearch = '';
            }).catch((error) => console.log(error));
        },

        open () {
            this.showPopover = true;
        },

        close () {
            this.showPopover = false;
        },

        openTime () {
            this.showTimer = true;
        },

        openDuration () {
            this.showDuration = true;
        },

        openTicket () {
            this.showTicket = true;
        },

        openService () {
            this.showService = true;
        },

        closeTime () {
            this.showTimer = false;
        },

        closeDuration () {
            this.showDuration = false;
        },

         closeTicket () {
            this.showTicket = false;
        },

        closeService () {
            this.showService = false;
        },

        getList () {

            this.hasItems = true;
            this.items = this.optionGroups.hotelWithCity;
            var vm = this;
            var results = {};
            if(this.useType == 2 && vm.name != '') {
                this.cityName = false;
                results = _.filter(this.optionGroups.hotelList,function(item){
                    let langu = 'city_name_'+Vue.config.lang;
                return item[langu].toLowerCase() == vm.name.toLowerCase() && (item.service_applied == 1 || item.service_applied == 2);
                });
                this.items = results;
            } else if( vm.name != '') {
                 this.cityName = false;
                results = _.filter(this.optionGroups.hotelList,function(item){
                    let langu = 'city_name_'+Vue.config.lang;
                return item[langu].toLowerCase() == vm.name.toLowerCase() && (item.service_applied == 0 || item.service_applied == 2);
                });
                this.items = results;
            } 
            if(results.length == 0 && vm.name != '') {
                this.cityName = false;
                if(this.useType == 2)
                    results = _.filter(this.optionGroups.hotelList,function(item){
                    return item.hotel_name.toLowerCase().indexOf(vm.name.toLowerCase()) >-1 && (item.service_applied == 1 || item.service_applied == 2);
                    });
                else
                   results = _.filter(this.optionGroups.hotelList,function(item){
                    return item.hotel_name.toLowerCase().indexOf(vm.name.toLowerCase()) >-1 && (item.service_applied == 0 || item.service_applied == 2);
                    });

                return this.items = results;
            } else if(results.length > 0) {
                this.cityName = false;
                return this.items = results;
            }
            if(vm.name == '') {
                this.cityName = true;
               return this.items = this.optionGroups.hotelWithCity;
            }
            
        },


        getListUpdated(){

        },

        dateChanged () {
            this.serviceName = 'Services'
            this.serviceId = -1;
            this.serviceIcon = '../../../static/images/services.png'

            this.$nextTick(function () {
                this.name = '';
                this.hotelName = ''; 
                this.id = ''; 
                this.hasItems = false;
                /*
                when switch day/night make list empty
                */
                this.optionGroups = [];
                this.getGroupHotel();
                if (this.useType == 1) {
                    this.getHotelAvailability('white_brand', false);
                    $("#check-in-date").text(moment().format('DD.MM.YYYY'));
                    $("#check-out").text(moment().format('DD.MM.YYYY'));
                    this.minStay = 0;
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().format('YYYY-MM-DD');
                } else if (this.useType == 2) {
                    this.getHotelAvailability('white_brand', true);
                    $("#check-in-date").text(moment().format('DD.MM.YYYY'));
                    $("#check-out").text(moment().add(1, "days").format('DD.MM.YYYY'));
                    this.checkin = moment().format('YYYY-MM-DD');
                    this.checkout = moment().add(1, "days").format('YYYY-MM-DD');
                }
                
            });
        },

        toggle(){
            return this.useType = this.useType == 1 ? 2 : 1;
        },

         validateCheckInDate () {
            let i = 0;
            let closedDate = 0;
            let validateDate = false;
            while (! validateDate && i <= 365) {
                let isAvailable = moment(this.checkin).add(i, "days").format('YYYY-MM-DD');
                if (this.availableDateArray[isAvailable] == undefined) {
                    closedDate = i+1;
                    i++;
                    continue;
                } else {
                    if (this.useType == 1) {
                        this.checkin = this.getUrlParameter('check_in');
                        this.checkout = this.getUrlParameter('check_out');

                        if(!closedDate){
                        if(this.checkin){
                        $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                        }else{
                            $("#check-in-date").text(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                            this.checkin = moment().add(closedDate, "days").format('YYYY-MM-DD')
                        }

                        if(this.checkout){
                         $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                        }else{
                         $("#check-out").text(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                         this.checkout = moment().add(closedDate, "days").format('YYYY-MM-DD')
                        }
                    }else{

                        $("#check-in-date").text(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                        this.checkin = moment().add(closedDate, "days").format('YYYY-MM-DD')
                         $("#check-out").text(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                         this.checkout = moment().add(closedDate, "days").format('YYYY-MM-DD')
                    }
                        this.getHotelCheckInHours('white_brand');
                        validateDate = true;
                    } else if (this.useType == 2) {
                        this.checkin = this.getUrlParameter('check_in');
                        if(this.checkin){
                        $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                        }else{
                            $("#check-in-date").text(moment().add(closedDate, "days").format('DD.MM.YYYY'));
                            this.checkin = moment().add(closedDate, "days").format('YYYY-MM-DD')
                        }
                        let vm = this;setTimeout(function(){vm.minimumStay() }, 300);
                        //this.minimumStayValidatingClosedDates(closedDate) ;
                        //this.minimumStay()
                        this.checkout = this.getUrlParameter('check_out');
                        if(this.checkout){
                            if(this.checkin === this.checkout){
                                $("#check-out").text(moment(this.checkout).add(this.min_stays,'days').format('DD.MM.YYYY'));
                            }else{
                                $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                            }
                        }else{
                         $("#check-out").text(moment().add(closedDate + this.minStay, "days").format('DD.MM.YYYY'));
                         this.checkout = moment().add(closedDate + this.minStay, "days").format('YYYY-MM-DD')
                        }
                    }
                    i++;
                }
               
                if( i >= this.minStay && this.useType ) {
                    validateDate = true;
                }
            }
        },

        addRoom () {
            this.rooms.push({ guest: 1 });
            this.guestCount();
        },

        removeRoom (index) {
            this.rooms.splice(index, 1);
            this.guestCount();
        },

        onSelected (date, type) {
            if (date) {
                if (this.useType == 1) {
                    if (type === 'checkin') {
                        this.checkin = moment(date).format('YYYY-MM-DD');
                        this.checkout = moment(date).format('YYYY-MM-DD');
                        $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                        $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                    } else if (type === 'checkout') {
                        this.checkin = moment(date).format('YYYY-MM-DD');
                        this.checkout = moment(date).format('YYYY-MM-DD');
                        $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                        $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                    }
                    this.serviceError = false;
                    this.getHotelCheckInHours('white_brand');
                } else if (this.useType == 2) {
                    if (type == 'checkin') {
                        this.checkin = moment(date).format('YYYY-MM-DD');

                        if(this.name && this.availableHotel) this.minimumStay();
                        else this.minimumStayForGroup();

                        if (date >= new Date(this.checkout)) {
                            if(this.name && this.availableHotel) this.minimumStay();
                            else this.minimumStayForGroup();
                            
                            this.checkout = moment(date).add(this.minStay, "days").format('YYYY-MM-DD');
                            $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                            $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                        } else {
                            this.checkin = moment(date).format('YYYY-MM-DD');
                            $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));

                            this.checkout = moment(date).add(this.minStay, "days").format('YYYY-MM-DD');
                            $("#check-in-date").text(moment(this.checkin).format('DD.MM.YYYY'));
                            $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                        }
                    } else if (type == 'checkout') {
                        if (date <= new Date(this.checkin)) {
                            if(this.name && this.availableHotel) this.minimumStay();
                            else this.minimumStayForGroup();    

                            this.checkout = moment(date).add(this.minStay, "days").format('YYYY-MM-DD');
                            $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                        }else {
                            this.checkout = moment(date).format('YYYY-MM-DD');
                            $("#check-out").text(moment(this.checkout).format('DD.MM.YYYY'));
                        }
                    }
                } else {
                    $("#check-in-date").text(this.checkin);
                    $("#check-out").text(this.checkout);
                }
            }
            let num_of_day = moment(this.checkout).diff(moment(this.checkin), 'days');
            var vm = this;
            for (var i = 1; i < num_of_day; i++) {
                var isAvailable = moment(this.checkin).add(i, "days").format('YYYY-MM-DD');
                if (vm.availableDateArray[isAvailable] == undefined) {
                    vm.disableSearch = 'disabledSearch';
                    vm.dateRangeError = true;
                    return;
                } else {
                    vm.disableSearch = '';
                    vm.dateRangeError = false;
                }
            }
        },

        input (date) {
            console.log('input--' + date);
        },

        guestCount () {
            this.showRoom = false;
            var totalGuest = 0;
            var totalRoom = 0;
            $.each(this.rooms, function (value, key) {
                totalGuest += parseInt(key.guest);
                totalRoom += 1;
            });
            this.guest = totalGuest;
            this.room = totalRoom;
            if (totalGuest > 1) {
                this.guestText = this.$t('message.GUESTS_SMALL')+',';
            } else {
                this.guestText = this.$t('message.GUEST_SMALL')+',';
            }
            if (totalRoom > 1) {
                this.roomText = this.$t('message.ROOMS_SMALL');
            } else {
                this.roomText = this.$t('message.ROOM_SMALL');
            }
        },
        
        // Hotel search handler
        hotelSearchHandler () {
             this.serviceError = false;
            if (this.groupId == '' && this.name == '') {
                this.destinationError = true; return false;
            }else if (this.id == '' && this.name != '') {
                this.destinationWrong = true; return false;
            } else if (this.useType == 1 && (this.check_in_time == '' || this.check_in_time == '00:00')) {
                this.checkInTimeError = true; return false;
            } else if (this.useType == 2 && (this.checkin == '' || this.checkout == '')) {
                this.checkInTimeError = true; return false;
            }else if(this.serviceId == -1 && this.useType == 1){
            this.serviceError = true;
            this.disableSearch =   'disabledSearch';
            return false;
            }
            
            let checkin = $('[name="checkin"]').val(),
                checkout = $('[name="checkout"]').val(),
                mode = this.useType;
            
            let roomArr  = [];
            $.each(this.rooms, function (value, key) {
                roomArr.push(key.guest);
            });
            /*Mouse flow code*/
                window._mfq = window._mfq || [];
                window._mfq.push(['formSubmitAttempt', '#searchForm']);

                let roomAr = [];
            let params = {
                "use_type":  mode,
                "check_in":  moment(this.checkin).format('YYYY-MM-DD'),
                "check_out": (mode==1) ? moment(this.checkin).format('YYYY-MM-DD') : this.checkin === this.checkout ? moment(this.checkout).add(this.min_stays,'days').format('YYYY-MM-DD'):moment(this.checkout).format('YYYY-MM-DD'),
                "rooms[]": this.serviceId > 0 && this.unit == 'hourly'?roomAr.push(1):roomArr,
            };
            
            if(mode == 1) params['check_in_time'] = this.check_in_time;
            if (mode == 1) params['check_out_time'] = this.check_out_time;
            if(mode == 1) params['duration'] = this.duration;

            if (mode == 1 && this.unit=='ticket') {
                params['ticket'] = this.ticket;
            }else{
                params['ticket'] = 1;
            }
            if (mode == 1) params['service_id'] = this.serviceId;
            if (mode == 1) params['service_name'] = this.serviceName;
            if (mode == 1) params['service_icon'] = this.serviceIcon;
            if(mode==1) this.$store.commit('setServiceType', this.serviceId);  
           
             params['unit'] = this.unit;
            if(this.activepage == 'home') {
                params['group'] = this.domainDetails.id;
            } else {
                params['group'] = this.groupId;
            }
             params['from'] = params['group'];
             params['service_applied'] = this.groupServiceApplied;
            let route = (mode == 2) ? this.white_label_url+'/night-use-search' : this.white_label_url+'/day-use-search';
            
            params = $.param( params );

            window.location.href = route + '?' + params;
        },
         setServiceId(serviceId){
            this.checkInTimeError = false;
        let indexes = _.findIndex(this.serviceData, { 'serviceId': serviceId });
        let hoursListDatas = this.serviceData[indexes];
        this.unit = hoursListDatas ? hoursListDatas.unit : null;
        this.hoursList = hoursListDatas ? hoursListDatas.check_in : null;
        this.hoursApiRes = hoursListDatas;
        this.check_in_time = this.hoursList[0] ? this.hoursList[0] : '00:00';
        if(serviceId==0){
            this.check_out_time = hoursListDatas.day_use_checkout ? hoursListDatas.day_use_checkout : '00:00';
        }else{
            this.check_out_time = hoursListDatas.check_out ? hoursListDatas.check_out : '00:00';
        }
        this.serviceName = hoursListDatas.serviceName;
        this.serviceIcon = hoursListDatas.service_icon;
        this.serviceId = serviceId;
        this.disableSearch = '';
        // if(this.unit != 'ticket'){
            this.minMaxCalculation(1);
         // }
        this.$store.commit('setServiceType', this.serviceId);  
        if(serviceId > -1){
            this.serviceError = false;
            this.disableSearch =   '';
        }
        this.closeService();
      },

      setTicket(ticket){
        this.ticket = ticket;
        this.closeTicket();
      },

      removeEmptyRoom(indexes,index){
        var removeRoom = this.serviceData[index];
        var min_max_rooms = removeRoom.min_max_rooms;
      }
    }

}
