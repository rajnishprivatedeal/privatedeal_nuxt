import login from '@/components/Login.vue'
import logout from '@/components/Logout.vue'
import registration from '@/components/Registration.vue'
import signUpMsg from '@/components/SignUpMsg.vue'
import cancelBooking from '@/components/CancelBooking.vue'
import cancelBookingOtpVerification from '@/components/CancelBookingOtpVerification.vue'
import cancelMsg from '@/components/CancelMsg.vue'
import { HotelDetails } from '@/mixins/hotel-details.js'


export default {

    name: 'FooterSection',

    mixins: [ HotelDetails ],
    
    data() {
        return {
            countries: null,
            subdomain: null,
            canceledEmailId:null,
            canceledBookingId:null,
            showModal: false,
            showCancel: false,
            showLoginModal: false,
            account_created: false,
            showRegistrationModal: false,
            showCancelOtpVerification:false,
            isgroup: null
        }       
    },
    
    components: {
        cancelBooking,
        cancelMsg,
        login,
        registration,
        logout,
        signUpMsg,
        cancelBookingOtpVerification
    },
    
    methods: {
        
        onClose() {
            this.showModal =  false;
            this.showCancel = false;
            this.showLoginModal =  false;
            this.showRegistrationModal =  false;
            this.account_created = false;
            this.showCancelOtpVerification = false;
        },

        onCancelClose(canceled_email_id,canceled_booking_id) { 
            this.canceledBookingId = canceled_booking_id;
            this.canceledEmailId = canceled_email_id;
            this.showModal =  false;
            this.showCancelOtpVerification =true;                
            // this.showCancel = true;
        },

        showCancelModal(){
            this.showModal = true;
        },

        onRegistrationClose() {
            this.showLoginModal =  false;
            this.showRegistrationModal =  false;
            this.showHelpModal = false;
            this.account_created = true;
        },

        onLogin(){
            this.showRegistrationModal = false;
            this.showLoginModal = true;
            this.showHelpModal = false;
        },

        onRegistraion(){                
            this.showLoginModal =  false;
            this.showRegistrationModal = true;
        },

        
        onCancelOtpClose(){
            console.log('show cancel message');
            this.showCancelOtpVerification = false;
            this.showCancel = true;
        },
    },

    computed: {

        acceptOnlinePayment() {
               return this.$store.getters.acceptOnlinePayment;
        },

        url () {
            if(this.domainDetails === null) return '';
    
            return 'background-color:' + this.domainDetails.layout_color + ' !important;';
        },

        language() {
            return this.$store.getters.getLanguage;
        },
        
        font_color () {
            var domainColor = "color:'#000';";

            if (this.domainDetails !== null) domainColor = "color:" + this.domainDetails.font_color;

            return domainColor; 
        },

        day_use () {
            if (this.groupId) return true;
            else if (this.domainDetails !== null && this.hotel) return this.hotel.service_applied !== '1' && this.hotel.accept_online_payment == 0;
        },
        
        ishome() {
           /* if(this.$route.name == 'payment-successful' || this.$route.name == 'night-use-offer-accepted' || this.$route.name == 'day-use-accept' || this.$route.name == 'day-use-offer-accepted')
                return true;*/
            return false;
        },

        user(){
            return this.$store.getters.token !=null ? true : false
        },
        hotelInfo() {
            return this.$store.getters.hotel;
        }
    },
    
    mounted(){
        console.log(this.$route.name)
    }
}