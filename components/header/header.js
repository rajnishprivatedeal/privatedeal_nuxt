import Vue from 'vue'
import login from '@/components/Login.vue'
import logout from '@/components/Logout.vue'
import registration from '@/components/Registration.vue'
import help from '@/components/Help.vue'
import signUpMsg from '@/components/SignUpMsg.vue'
import { parseNumber, formatNumber, AsYouType } from 'libphonenumber-js'
import { HotelDetails } from '@/mixins/hotel-details.js';
import { serverBus } from '@/main';
import { Constants } from '@/config/constants.js';
// import 'font-awesome/css/font-awesome.min.css'
const bus = new Vue();

export default {
    components: { login,  logout, help, signUpMsg,registration },

    mixins: [HotelDetails],

    data() {
        return {
            showLoginModal: false,
            account_created: false,
            languagesObject:{'de':'Deutsch','en':'English','es':'Español','fr':'Français'},
            showRegistrationModal: false,
            showHelpModal: false,
            homePageClass: '',
            currencies: [],
            data: this.subdomain,
            sliderLocale:null,
            tracking_id:[]
        }
    },
    
    mounted: function () {
        var token = this.getCookie("new_token");
        if(token) this.$store.commit('setToken', token);
        this.$nextTick(function () { 
            if (this.activepage === 'home' || this.activepage === 'global-search') {
                this.homePageClass = "navbar navbar-default navbar-fixed-top menu-top";
            } else {
                this.homePageClass = "navbar navbar-default navbar-fixed-top menu-top";
                // if (this.domainDetails) this.stickyColor = "background-color:" + this.domainDetails.layout_color + " !important;"
            }
            
            if (this.domain == 'group') $("body").append("<style>* { font-family: " + this.domainDetails.font + ";}</style>");
            else if (this.domainDetails) $("body").append("<style>* { font-family: " + this.domainDetails.hotel_font + ";}</style>");
            //if (this.domainDetails) this.setGtagCode(this.domainDetails);
            // Get currencies list
            this.getCurrency();
        });
        
       
        serverBus.$on('logout' , (str) => {
            this.eraseCookie("new_token");
            this.eraseCookie("laravel_session");
            this.eraseCookie("XSRF-TOKEN");
            this.$store.commit('setToken', null);
            this.authLogout();
            
            this.getHotelDetails();
        });

        if(this.language) {
            this.$store.commit('setLanguage', this.language);
            // if($zopim.livechat != undefined)
            //     $zopim.livechat.setLanguage(this.language);
        }

    },

    created() {
        this.setResetState();
    },

    destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    },

    methods: {
         downloadJSAtOnload() {
            var language = this.language ? this.language: 'en';
           
            // $.getScript(Constants.httpProtocol+Constants.hostName+"/zopim_defer.js", function(data, textStatus, jqxhr){
            //     $zopim(function()
            //         {
            //             if (typeof($zopim.livechat) == 'object')
            //             {  
            //                 var url = window.location.href.split('/');
            //                 if(url.length >= 4 && (url[3] == 'en' || url[3] == 'fr' || url[3] == 'de'  || url[3] == 'es' )){
            //                    $zopim.livechat.setLanguage(url[3]);
            //                 } else {
            //                     $zopim.livechat.setLanguage(language);
            //                 }
            //             }
            //         }
            //     );
            // });
        },

        getLocale(){
          if (this.activepage == 'home' ){
              var url = window.location.href.split('/');
              if(url.length >= 4 && (url[3] == 'en' || url[3] == 'fr' || url[3] == 'de'  || url[3] == 'es' )){
                  this.sliderLocale = url[3]
                  var cookieVal = this.getGCookie('glanguage');
                  if(!cookieVal){
                   this.$store.commit('setLanguage', this.sliderLocale);
                }else{
                   this.$store.commit('setLanguage', cookieVal); 
                }
              } 
            }  
        },

        async setResetState() {
            // Get hotel details
            if (this.$store.getters.activePage == 'home') {

                await this.$store.commit('resetHomePageState'); 

                await this.getHotelDetailsInHotelLang();
                
            } else if (!this.domainDetails) { 

                await this.getHotelDetails();

            }
            
            if (!this.domainDetails) {
                this.$store.commit('setLanguage', 'en');
                this.$store.commit('setCurrency', 'CHF');
            }
            
            window.addEventListener('scroll', this.handleScroll);
            
            if(this.domain == 'group') {
                this.setLangCookie( this.language );
                this.setCurrencyCookie( this.currency );
                serverBus.$emit('change-group-language', this.language);
            }
        },

        getCookie(name) {
            var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
            return v ? v[2] : null;
        },
        getGCookie(cname) {
                var v = document.cookie.match('(^|;) ?' + cname + '=([^;]*)(;|$)');
                return v ? v[2] : null;
            },
            setGCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            },

        getUrlParameter (name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },

        setLangCookie( lang ) {
            // document.cookie = "language=" + lang + ";domain=localhost;path=/;";
            document.cookie = "language=" + lang + ";domain="+Constants.hostName+";path=/;";
        },

        setCurrencyCookie( currency ) {
            document.cookie = "currency=" + currency + ";domain="+Constants.hostName+";path=/;";
        },

        eraseCookie(name) {
            document.cookie = name+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain="+ Constants.hostName +";path=/"; 
        },
        
        authLogout() {

            let logoutAPI = '/auth-logout'; 
            
            this.$axios.get(logoutAPI).then((response) => {
                
                window.location.href = '/';

            }).catch((error) => console.log(error));
        },
        
        // Get hotel details in hotel language
        async getHotelDetailsInHotelLang (language = '') {

            try {
                
                this.getLocale();

                let indexAPI = this.sliderLocale ? '/index/' + this.sliderLocale : '/index'; 
                var cookieVal = this.getGCookie('glanguage');
                if(cookieVal){
                    indexAPI = cookieVal ? '/index/' + cookieVal: '/index'; 
                }
                //let indexAPI = '/index'; 
               
                let response = await this.$axios.get(indexAPI);
                
                if (response.data) {
                    let result = response.data.data;
                    if (result) { 
                        this.setGtagCode(result.subdomain.details);                     
                    }
                    // Setting whitebrand hotel details
                    await this.$store.commit('setWBHotelDetails', result);
                    if (result.subdomain.domainType == 'group') $("body").append("<style>* { font-family: " + result.subdomain.details.font + ";}</style>");
                    else if (result) $("body").append("<style>* { font-family: " + result.subdomain.details.hotel_font + ";}</style>");
                    // this.downloadJSAtOnload();
                }
            
            } catch (error) {
                this.$store.commit('setIsDeactive', 1);
                console.log(error);
            }
        },

        // Get hotel details
        async getHotelDetails (language = '') {

            try {
                var vm = this;
                this.getLocale();
                // let indexAPI = language ? '/' + language + '/index' : '/index'; 
                // this.setGCookie('glanguage',language,365);
               
                let indexAPI = this.language ? '/index/' + this.language : '/index'; 
                var cookieVal = this.getGCookie('glanguage');
                if(cookieVal){
                    indexAPI = cookieVal ? '/index/' + cookieVal: '/index'; 
                }

                let response = await this.$axios.get(indexAPI);
                
                if (response.data) {
                    let result = response.data.data;
                    // Setting whitebrand hotel details
                    await this.$store.commit('setWBHotelDetails', result);
                    vm.setGtagCode(result.subdomain.details);
                }
            
            } catch (error) {
                this.$store.commit('setIsDeactive', 1);
                console.log(error);
            }
        },

        setGtagCode(details) {
            if(details.tracking_id != null) {
                let tracking_ids = details.tracking_id;
                let analyticsArray = tracking_ids.split(",");
                if(analyticsArray.length > 1) {
                    this.tracking_id = analyticsArray;
                    for( let i = 0; i < this.tracking_id.length; i++) {
                        if(i == 0 ) {
                            window.tracking_id = this.tracking_id[i];
                            var googleAnalytics = document.createElement('script');
                            googleAnalytics.async = true;
                            googleAnalytics.src = "https://www.googletagmanager.com/gtag/js?id="+this.tracking_id[i];
                            document.getElementsByTagName('head')[0].appendChild(googleAnalytics);
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments)};
                            gtag('js', new Date());
                            gtag('config',window.tracking_id);
                        } else {
                            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                            })(window,document,'script','dataLayer',this.tracking_id[i]);
                        }
                    }
                } else {
                    window.tracking_id = details.tracking_id;
                    var googleAnalytics = document.createElement('script');
                    googleAnalytics.async = true;
                    googleAnalytics.src = "https://www.googletagmanager.com/gtag/js?id="+details.tracking_id;
                    document.getElementsByTagName('head')[0].appendChild(googleAnalytics);
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments)};
                    gtag('js', new Date());
                    gtag('config',window.tracking_id);
                }
            }
        },

        handleScroll() {
            if (this.activepage === 'home' && this.subdomain === 0) {
                this.scrolled = window.scrollY > 120;
                if (window.scrollY > 120) {
                    $("nav").addClass("sticky");
                } else {
                    $("nav").removeClass("sticky");
                }
                if (window.scrollY > 120) {
                    if (this.data) {
                        // this.stickyColor = "background-color:" + this.data.layout_color + " !important;"
                    }
                }
            }
        },

        onClose() {
            this.showLoginModal = false;
            this.showRegistrationModal = false;
            this.showHelpModal = false;
            this.account_created = false;
        },

        onRegistrationClose() {
            this.showLoginModal = false;
            this.showRegistrationModal = false;
            this.account_created = true;
        },

        onLogin() {
            this.showRegistrationModal = false;
            this.showLoginModal = true;
        },

        onHelp() {
            this.showRegistrationModal = false;
            this.showLoginModal = false;
            this.showHelpModal = true;
        },

        onRegistraion() {
            this.showLoginModal = false;
            this.showRegistrationModal = true;
        },

        getCurrency() {
           
            let indexAPI = this.language ? '/currency-list/' + this.language : '/currency-list'; 
            var cookieVal = this.getGCookie('glanguage');
                if(cookieVal){
                    indexAPI = cookieVal ? '/currency-list/' + cookieVal: '/currency-list'; 
                }
            this.$axios.get(indexAPI).then((response) => {

                this.currencies = response.data.data;

            }).catch((error) => console.log(error));
        },
        
        onLogoClick() {
            let previousLang = this.language ? this.language : ''; 
            if(this.$store.getters.activePage == 'home') {
                window.location.href = this.website;
            } else {
                this.$router.push('/'+previousLang)
            }
        },

        onGroupLogoClick() {
         let previousLang = this.language ? this.language : ''; 
            if(this.$store.getters.activePage == 'home') {
                window.location.href = this.groupWebsite;
            } else {
                this.$router.push('/'+previousLang)
            }
        },

        async getLanguages(){
           
            try {
                await this.$axios.get('/languages-list').then((response) => {
                    if(response.data.data) {
                        this.languagesObject = response.data.data; 
                    }
                })
            } catch (error) {
                console.log(error);
            }
        },
         serviceChange(lang){

        },
        changeHotelCurrency(currency){
            this.$store.commit('setCurrency', currency);
        },
        setHotelLanguage(lang){
            // console.log(this.$i18n)
            this.$i18n._vm.locale = lang;
            this.$store.commit('setLanguage', lang);
            this.$router.push({path:lang});
        }

    },

    computed: {
        
        currency: {
            get () {
                var cookieVal = this.getGCookie('gcurrency');
                if(cookieVal){
                    this.$store.commit('setCurrency', cookieVal);
                }
                return this.$store.getters.getCurrency;
            },
            set (value) {
                this.setGCookie('gcurrency', value, 356)
                this.$store.commit('setCurrency', value);
                
                // Emit currency change event
                this.$emit('changeCurrency', value);
                
                //set currency cookie
                if(this.domain == 'group' || this.getUrlParameter('group')) this.setCurrencyCookie( value );
            }
        },
        
        language: {
            get () {
                var cookieVal = this.getGCookie('glanguage');
                if(cookieVal){
                   this.$store.commit('setLanguage', cookieVal);
                }
                return this.$store.getters.getLanguage;
            },
            set (value) {
                
                /*Zendesk language change*/
                // if($zopim.livechat != undefined) $zopim.livechat.setLanguage(value);
                if(this.$store.getters.activePage == 'home')
                    this.$router.push({ name: "welcome", params: {locale:value}})
                this.$store.commit('setLanguage', value);
                this.setGCookie('glanguage', value, 356);
                // this.language = this.getGCookie('glanguage');
                // Emit change language event
                this.$emit('changeLang', value);
                this.$i18n._vm.locale = value;
                // Get whitebrand hotel details corresponding to language
                this.getHotelDetails(value);

                // get currency list in language
                this.getCurrency();

                serverBus.$emit('change-room-language', value);

                //set language cookie
                if(this.domain == 'group' || this.getUrlParameter('group')) this.setLangCookie( value );

                if(this.domain == 'group' || this.getUrlParameter('group')) serverBus.$emit('change-group-language', value);
                
            }
        },

        stickyColor () {
            if(this.$store.getters.activePage == 'hotel') {
                
                if(this.domainDetails === null) return '';
        
                return 'background-color:' + this.domainDetails.layout_color + ' !important;';
            }
            return '';
        },

        logo () {

            var url = Constants.filePath + '/img/logo.png';
            
            return url;
        },

        url () {

            var url = 'background:url(' + Constants.filePath + '/uploads/';
            
            if (this.domainDetails !== null) url += this.domainDetails.hotel_logo;

            url += ') no-repeat center top/contain;';
            // <====added dynamic hotel icon and name start====>
            var dynamicLogo = Constants.filePath + '/uploads/'+this.domainDetails.hotel_logo;
            $('.hotelIcon').attr('href',dynamicLogo);
            $('.hotelTitle').html(this.hotel.hotel_name);
            document.title = this.hotel.hotel_name;
            // <====added dynamic hotel icon and name end====>
            return url;
        },

        groupUrl () {
            
            var url = 'background:url(' + Constants.filePath + '/uploads/';
            
            if (this.domainDetails !== null) url += this.domainDetails.logo;

            url += ') no-repeat center top/contain;';
            document.title = this.domainDetails.title;
            return url;
        },

        subdomaincolor() {
            let subdomaincolor = "color:'';";

            if (this.domainDetails) subdomaincolor = "background-color:#772583;";

            return subdomaincolor;
        },

        font_color() {
            let subdomaincolor = "color:'#000';";

            if (this.domainDetails) subdomaincolor = "color:" + this.domainDetails.font_color+' !important;';

            return subdomaincolor;
        },

        website() {
            let url;

            if (!/^(f|ht)tps?:\/\//i.test(this.hotel.website)) return url = "http://" + this.hotel.website;

            return this.hotel.website;
        },

        groupWebsite() {
            let url;

            if (!/^(f|ht)tps?:\/\//i.test(this.domainDetails.website)) return url = "http://" + this.domainDetails.website;

            return this.domainDetails.website;
        },
        hotelInfo() {
            return this.$store.getters.hotel;
        },
         user(){
            return this.$store.getters.token !=null ? true : false
        }

    },

    filters: {
        phone: function (phone) {
            let asYouType = new AsYouType();
            return asYouType.input('+' + phone);
        }
    }


}


