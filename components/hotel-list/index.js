import HotelSearch from  '@/components/search/search.vue';
import whiteLabel from  '@/components/white-brand-bid/WhiteLabel.vue';
import GroupHotelSearch from  '@/components/search-group/search-group.vue';
// import router from '@/router.js'
import { HotelDetails } from '@/mixins/hotel-details.js';
import { BidDetails } from '@/mixins/bid-details.js';
import { Constants } from '@/config/constants.js'

export default {

  name: 'HotelList',
  
  mixins: [ HotelDetails, BidDetails ],
  
  components: { HotelSearch, whiteLabel, GroupHotelSearch},
  // components: { HotelSearch, whiteLabel },
  
    data () {
        return {
            showLoader: true,
            typeId: null,
            name: null,
            homeIcon: 1,
            dateDisabled:false,
            searchCriteria: {},
            searchedFor: {},
            searchResult: {},
            showResult: false,
            taxInformation:[],
            hotels: [],
            group:'',
            data:'',
            newbiddingprice:0,
            priviousbidhotellist:0,
            withoutOffer:'',
            hotelAuctions:[],
            selectedHotels:[],
            scrolled:false,
            disabled:false,
            proposal:'btn btn-proposal text-center',
            orderlist:[],
            showModal:false,
            showBidModal:false,
            reviewData:'',
            makeYourOffer:true,
            center: {lat:0, lng:0},
            markers: [],
            mapView:false,
            infoContent:{
                name:'',
                slug:''
            },
            infoWindowPos: { 
                lat: 0, 
                lng: 0 
            },
            infoWinOpen: false,
            infoOptions: {
                pixelOffset: { 
                    width: 0, 
                    height: -25
                }
            },
            selectedForOffer:false,
            dragOption:{ group :{ "pull":"clone", "put":false }},
        }
    },

    computed: {
          
          url() {

              if(this.hotel) {

                  let website = this.hotel.website;

                  var url = 'background:url(' + Constants.filePath + '/uploads/';
                  
                  if (this.hotel !== null) url += this.hotel.primary_image;

                  //url += ') center center / cover no-repeat;';
                  url += ');';

                  return url;
              }

              return '';
          },

          no_of_day() {
              var numberOfDays;
              let num_of_day = 0;
              if(this.searchCriteria.hasOwnProperty('check_in') && this.searchCriteria.hasOwnProperty('check_out')){
                  let check_in  = this.searchCriteria['check_in'];
                  let check_out = this.searchCriteria['check_out'];
                  let mode = this.searchCriteria['use_type'];
                  if(mode == 1 && this.searchCriteria['duration'] == 1 ) return numberOfDays = '1 '+this.$t('message.HOUR_SMALL');
                  else if(mode == 1 && this.searchCriteria['duration'] > 1) return numberOfDays = this.searchCriteria['duration']+' '+this.$t('message.HOURS_SMALL');
                  numberOfDays = moment(check_out).diff(moment(check_in), 'days');
                  if(numberOfDays>1){
                      numberOfDays = numberOfDays+' '+ this.$t('message.NIGHTS_SMALL');
                  }else{
                      numberOfDays = numberOfDays+' '+ this.$t('message.NIGHT_SMALL');
                  }
                  return numberOfDays;
              }
              return '';
          },

          no_of_guest() {
              let totalGuest = 0, noOfGuest = '';
              if(this.searchCriteria.hasOwnProperty('rooms[]')){
                  let rooms  = this.searchCriteria['rooms[]'];
                  if(Array.isArray(rooms)) {
                      rooms.forEach(function(value, key) {
                          totalGuest += parseInt(value);
                      });
                  } else {
                      totalGuest = parseInt(rooms);
                  }
                  if (totalGuest > 1) noOfGuest = totalGuest+' '+this.$t('message.GUESTS_SMALL');
                  else noOfGuest = '1 '+this.$t('message.GUEST_SMALL');
                  return noOfGuest;
              }
              return '';
          },

          no_of_room() {
              let totalRoom = 0, noOfRoom = '' ;
              if(this.searchCriteria.hasOwnProperty('rooms[]')){
                  let rooms  = this.searchCriteria['rooms[]'];
                  if(Array.isArray(rooms)) {
                      rooms.forEach(function(value, key) {
                          totalRoom += 1;
                      });
                  } else {
                      totalRoom = 1;
                  }
                  if (totalRoom > 1) noOfRoom = totalRoom+' '+this.$t('message.ROOMS_SMALL');
                  else noOfRoom = '1 '+this.$t('message.ROOM_SMALL');
                  return noOfRoom;
              }
              return '';
          },

          tax_calculation(){
              //this.include = _.replace(this.$t('message.INCLUDING'), '(VAT)', this.taxInformation.vat );
              this.include = this.taxInformation.vat;
              if (this.searchCriteria.use_type == 2) {
                  if (this.hotel.city_tax_type == '1' && this.hotel.city_tax_included == '0') {
                      this.exclude = _.replace(this.$t('message.EXCLUDING'), '(currency)', this.currency+' '+this.taxInformation.city_tax );
                  } else if (this.hotel.city_tax_type == '2' && this.hotel.city_tax_included == '0')  {
                      this.exclude = _.replace(this.$t('message.EXCLUDING_PERCENTAGE'), '(currency)', this.taxInformation.city_tax+'%' );
                  } else if (this.hotel.city_tax_type == '1' && this.hotel.city_tax_included == '1'){
                      this.exclude = _.replace(this.$t('message.INCLUDING_PERCENTAGE'), '(currency)', this.currency+' '+this.taxInformation.city_tax );
                  } else if (this.hotel.city_tax_type == '2' && this.hotel.city_tax_included == '1'){
                      this.exclude = _.replace(this.$t('message.INCLUDING_PERCENTAGE'), '(currency)', this.taxInformation.city_tax+'%' );
                  }
                   if ( this.taxInformation.city_tax_type == '1') {
                  this.exclude = _.replace(this.exclude, '(citytax)',this.$t('message.TourismDirham')  );
                }else{
                  this.exclude = _.replace(this.exclude, '(citytax)',this.$t('message.CITY_TAX')  );
                }
                  if(this.hotel.city_tax_for_adults == 0 || this.hotel.city_tax_for_adults == '0') {
                    this.exclude = _.replace(this.exclude, '(per_person)',this.$t('message.PER_GUEST')  );
                  } else {
                       this.exclude = _.replace(this.exclude, '(per_person)',this.$t('message.PER_ADULT')  );
                  }
                  this.exclude = _.replace(this.exclude, '(night)',this.$t('message.NIGHT_SMALL')  );
              } else {
                  this.exclude = this.$t('message.DAY_USE_MESSAGE');
              }
              if(this.taxInformation.vat != 0 && this.hotel.is_vat && this.taxInformation.city_tax != 0 && this.hotel.is_city_tax)
                  return '('+this.include+' - '+this.exclude+')';
              else if( this.taxInformation.city_tax != 0 && this.hotel.is_city_tax)
                  return '('+this.exclude+')';
              else if(this.taxInformation.vat != 0 && this.hotel.is_vat )
                  return '('+this.include+')';
              else 
                  return '';
          },

          additional_tax_calculation(){
              if (this.searchCriteria.use_type == 2) {
                  if(this.hotel.is_additional_tax == '1' && this.hotel.additional_tax == null && this.hotel.additional_tax_included == '0' && this.hotel.additional_tax_for_adults == '0'){
                      this.exclude = this.$t('message.EXCLUDING_ADDITIONAL_TAX_PER_STAY');
                      return '('+this.exclude+')';
                  }
                  else if (this.hotel.additional_tax_type == '1' && this.hotel.additional_tax_included == '0') {
                      this.exclude = _.replace(this.$t('message.EXCLUDING_PERCENTAGE_ADDITIONAL_TAX'), '(currency)', this.currency+' '+this.taxInformation.additional_tax );
                  } else if (this.hotel.additional_tax_type == '2' && this.hotel.additional_tax_included == '0')  {
                      this.exclude = _.replace(this.$t('message.EXCLUDING_PERCENTAGE_ADDITIONAL_TAX'), '(currency)', this.taxInformation.additional_tax+'%' );
                  } else if (this.hotel.additional_tax_type == '1' && this.hotel.additional_tax_included == '1'){
                      this.exclude = _.replace(this.$t('message.INCLUDING_PERCENTAGE_ADDITIONAL_TAX'), '(currency)', this.currency+' '+this.taxInformation.additional_tax  );
                  } else if (this.hotel.additional_tax_type == '2' && this.hotel.additional_tax_included == '1'){
                      this.exclude = _.replace(this.$t('message.INCLUDING_PERCENTAGE_ADDITIONAL_TAX'), '(currency)', this.taxInformation.additional_tax+'%' );
                  }
                  this.exclude = _.replace(this.exclude, '(night)',this.$t('message.NIGHT_SMALL')  );
                  if(this.hotel.additional_tax_for_adults == '1') {
                      this.exclude = _.replace(this.exclude, '(per_person)',this.$t('message.PER_ROOM')  );
                      this.exclude = _.replace(this.exclude, '(and)',this.$t('message.AND')  );
                  }
                  else {
                      this.exclude = _.replace(this.exclude, '(per_person)',''  );
                      this.exclude = _.replace(this.exclude, '(and)',''  );
                  }
                  if(this.taxInformation.additional_tax != 0 && this.hotel.is_additional_tax )
                      return '('+this.exclude+')';


              }
              return '';
          },

          showMakeOfferButton(){
              var showButton = true;
              
              $.each(this.hotelAuctions, function(key, val){
                  if(!val.amount){
                      showButton = false;
                  }

              });
              
              return showButton;
          },

          layout_color(){
              var subdomaincolor = "color:'#000';";
              if(this.domainDetails){
                   subdomaincolor = "background-color:"+this.domainDetails.layout_color+" !important; color:"+this.domainDetails.font_color+"!important;"
              }
              return subdomaincolor; 
          },

          acceptOnlinePayment() {
               return this.$store.getters.acceptOnlinePayment;
        },

        day_check_out_date() {
               var checkOutDay;
              let day_check_out_date = 0;
              if(this.searchCriteria.hasOwnProperty('check_in')){
                  let check_in  = this.searchCriteria['check_in'];
                  let mode = this.searchCriteria['use_type'];
                  let duration = this.searchCriteria['duration'];
                  let check_in_time = this.searchCriteria['check_in_time'];
                  let check_in_datetime = moment(check_in + " " + check_in_time);
                  if(mode == 1){
                    checkOutDay = moment(check_in_datetime).add(duration, 'hours').format('DD.MM.YYYY');
                  }
                  return checkOutDay;
              }
              return '';
          },
          day_check_out_time() {
              var checkOutTime;
              if(this.searchCriteria.hasOwnProperty('check_in_time') && this.searchCriteria.hasOwnProperty('duration')){
                  let check_in  = this.searchCriteria['check_in'];
                  let mode = this.searchCriteria['use_type'];
                  let duration = this.searchCriteria['duration'];
                  let check_in_time = this.searchCriteria['check_in_time'];
                  if(mode == 1){
                    checkOutTime = moment(check_in_time, 'HH:mm').add(duration, 'hours').format('HH:mm');;
                  }
                  return checkOutTime;
              }
              return '';
          }

    },

    created () {
      
        let newBiddingPrice = this.$store.getters.getNewBiddingPrice;

        if(Object.keys(newBiddingPrice).length > 0) {
            history.pushState(null, null, location.href);
            window.onpopstate = function () {
                history.go(1);
            };
        }
        
        this.$store.commit('setActivePage', 'hotel');

        if (this.getUrlParameter('group')) {
            let groupLang = this.getCookie('language');
            let groupCurr = this.getCookie('currency');
            
            if(groupCurr) {
                this.$store.commit('setLanguage', groupLang);
            }
            
            if(groupLang) {
                this.$store.commit('setCurrency', groupCurr);
            }
        }
    },

     no_of_ticket() {
              let totalTicket = 0, noOfTicket = '' ;
              if(this.searchCriteria.hasOwnProperty('ticket')){
                  totalTicket  = this.searchCriteria['ticket'];
                  
                  if (totalTicket > 1) noOfTicket = totalTicket+' '+this.$t('message.TICKETS');
                  else noOfTicket = '1 '+this.$t('message.TICKETS');
                  return noOfTicket;
              }
              return '';
          },

          no_of_service() {
              let totalService = 0, noOfService = '' ;
              if(this.searchCriteria.hasOwnProperty('service_id')){
                  totalService  = this.searchCriteria['service_id'];
                  let unit = this.searchCriteria['unit'];
                  if(totalService > 0 && unit == 'hourly'){
                  return '1 Service'+this.$t('message.TICKETS');
                }
              }
              return '';
          },

    methods: {
      
      getCookie(name) {
          var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
          return v ? v[2] : null;
      },

      eraseCookie(name) {
          document.cookie = name+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain="+ Constants.hostName +";path=/"; 
      },

      onChangeLang(language) {
        this.$emit('changeLang', language);
          this.getHotelSearchList(this.searchCriteria)
      },

      onChangeCurrency(currency) {
          this.getHotelSearchList(this.searchCriteria)
      },

      resetAllSearch () {
          this.searchResult = {};
          this.searchedFor = {};
          this.taxInformation = [];
          this.withoutOffer = '';
      },
      
      emptyStorage () {
          this.$store.commit('setWithoutOffer', null);
          this.$store.commit('setSearchCriteria', {});
      },

      getHotelSearchList(parameters) {
          this.showResult = false;
          let roomJson = {}, roomParamArr=[],
                totalRoom = 0;
          //check rooms array exist or not
          if(parameters.hasOwnProperty('rooms[]')) {
              roomParamArr = parameters['rooms[]'];
              if(Array.isArray(roomParamArr)) {
                  roomParamArr.forEach(function(value, key) {
                      roomJson[++totalRoom] = parseInt(value);
                  });
              } else {
                  roomJson[++totalRoom] = parseInt(roomParamArr);
              }
          }
          if(!Object.keys(roomJson).length) this.$router.push({path: 'welcome'});
          
          //check use type is valid or not
          if(! parameters.hasOwnProperty('use_type')) this.$router.push({path: 'welcome'});
          let useType = parameters['use_type'];
          if (useType != 1 && useType != 2) this.$router.push({path: 'welcome'});
          
          //check checkin checkout is valid or not
          if(! parameters.hasOwnProperty('check_in') || ! parameters.hasOwnProperty('check_out')) this.$router.push({path: 'welcome'});
          let num_of_day = 0;
          let check_in  = parameters['check_in'];
          let check_out = parameters['check_out'];
          let duration = parameters['duration'];
          num_of_day = moment(check_out).diff(moment(check_in), 'days');
          if(isNaN(num_of_day)) this.$router.push({path: 'welcome'});
          if (useType == 1 && num_of_day != 0) this.$router.push({path: 'welcome'});
          if (useType == 2 && num_of_day < 1)  this.$router.push({path: 'welcome'});
          
          parameters['total_days'] = (useType == 2 ? num_of_day : (num_of_day)+1);
          this.searchCriteria = parameters;
          //set selected room stoage null
          this.emptyStorage();
          let limit_exceed_index = this.$store.getters.getLimitExceedRoom;
          this.$store.commit('setPLimitExceedRoom', limit_exceed_index);
          let params = {
              "use_type": useType,
              "check_in": check_in,
              "check_out": check_out,
              "rooms": roomJson,
              "currency_code": this.currency ? this.currency : 'CHF', //'USD',
              "guest_came_from": parameters['from'], //'USD',
              "language": this.language,
              "check_in_time": (useType == 1 ? parameters['check_in_time'] : ''),
              "duration": (useType == 1 ? parameters['duration'] : ''),
              'limit_exceed_index':limit_exceed_index
          };
          
          let apiURL = this.language ? '/' + this.language + '/search'  : '/en/search';
          this.$store.commit('setServiceType', parameters['service_id']); 
          // console.log(params)
          if(parameters['service_id'] > 0 && useType==1){
            params = {
              "use_type": useType,
              "check_in": check_in,
              "check_out": check_out,
              "rooms": roomJson,
              "currency_code": this.currency ? this.currency : 'CHF', //'USD',
              "guest_came_from": parameters['from'], //'USD',
              "language": this.language,
              "check_in_time": (useType == 1 ? parameters['check_in_time'] : ''),
              "duration": (useType == 1 && (parameters['unit'] == 'hourly' || parameters['service_id'] == 0) ? parameters['duration'] : ''),
              "units": (useType == 1 && (parameters['unit'] == 'ticket' || parameters['service_id'] != 0) ? parameters['ticket'] : ''),
              "service_type": (useType == 1 ? parameters['service_id'] : ''),
              'limit_exceed_index':limit_exceed_index
          };

            apiURL = this.language ? '/' + this.language + '/search-day-services'  : '/en/search-day-services';

          }
          // let apiURL = '/' + this.language + '/search' 

          try {
              this.$axios.post(apiURL, params).then((response) => {
                  this.showResult = true;
                  if (Object.keys(response.data.data.results).length) {
                      let data = response.data.data;
                      this.withoutOffer = data.without_offer;
                      this.searchResult = data.results;
                      if( Object.keys(this.$store.getters.getBidDetail).length > 0 ) {
                        let bidIndex = this.$store.getters.getBidDetail;
                        let index = (bidIndex.selected_search_id).split("-")[1];
                        for (var key in this.searchResult) {
                            if (this.searchResult.hasOwnProperty(key)) {
                              if ( key.split("-")[1] == index ) {
                                this.$store.commit('setSelectedRoom', this.searchResult[key].rooms);                                
                              }
                            }
                        }
                      }
                      this.searchedFor = data.searchedFor;
                      this.taxInformation = data.taxInformation;
                  } else {
                      this.resetAllSearch();
                  }
              }).catch(error => {
                  this.showResult = true;
                  this.resetAllSearch();
                  console.log('url errro');
              });
          } catch (error) {
              this.showResult = true;
              this.resetAllSearch();
              console.log('errro');
          }
      },
      
      async checkGroupExist() {
          
          if (this.getUrlParameter('group')) {

              let groupId = this.getUrlParameter('group');
              let apiURL = '/check-hotel-group';

              this.$store.commit('setGroupId', groupId);

              if(this.hotel.id == null || this.hotel.id == undefined) return;

              let params = {
                  "group_id": groupId,
                  "hotel_id": this.hotel.id,
              };
              console.log(params);

              await this.$axios.post(apiURL, params).then((response) => {
                  console.log(response);
                  this.$store.commit('setGroupId', groupId);
              }).catch(error => {
                  this.$store.commit('setGroupId', 0);
                  this.$router.push({path: 'welcome'});
              });

          } else {
              
              this.$store.commit('setGroupId', 0);
              
          }

      },

      getUrlParameter (name) {
          name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
          var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
          var results = regex.exec(location.search);
          return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
      }
      
    },
    
    watch: {
        '$route' : {
            deep: true,
            handler: function(to, from) {
                this.getHotelSearchList(to.query);
                // console.log(to.query)
            }
        }
    },
    
    beforeRouteEnter (to, from, next) {

        console.log('before route enter=>');
        console.log(to.query)
        next(vm => {
            vm.getHotelSearchList(to.query);
            next();
        });
    },

    mounted: function () {
        this.$nextTick(function () {

            if(window.innerWidth <= 767 ) {
                this.dragOption = { group :{ "pull":"clone", "put":false }, disabled: true};
            }
            
            if(this.subdomainData){
                this.layout_color = "background-color:"+this.subdomainData.layout_color+" !important; color:"+this.subdomainData.font_color+"!important;"
            }
            
            let newBiddingPrice = this.$store.getters.getNewBiddingPrice;

            if(Object.keys(newBiddingPrice).length) {
                this.dateDisabled = true;
                $("#currenyDropdown").addClass('disabledbutton' );
                $("#unitConversion").addClass('disabledbutton' );
            }
            
            this.checkGroupExist();
        })
    },

  filters:{ 

        moment: function (date) {
            return moment(date).format("DD MMM YYYY");
        },

        datemonth: function (date) {
            return moment(date).format('DD');
        },

        dayCheckInDate : function (date) {
            return moment(date).format('DD.MM.YYYY')
        }
    }

}