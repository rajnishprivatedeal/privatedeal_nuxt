import HotelSearch from  '@/components/search/search.vue';
import GroupHotelSearch from  '@/components/search-group/search-group.vue';
import { HotelDetails } from '@/mixins/hotel-details.js';


import Vue from 'vue';
let bus = new Vue();

export default {

  name: 'Welcome',

  mixins: [ HotelDetails ],
  components: { HotelSearch, GroupHotelSearch },

  data () {
      return {
        typeId: null,
        name: null,
        homeIcon: 0
      }
  },
  
  created () {
    
      this.setStorageEmpty();
      
  },

  mounted: function () {
        
  },

  methods: {
      
      setStorageEmpty() {
        
          //this.$store.commit('setDefaultCurrencyAndLanguage');
          
          this.$store.commit('setActivePage', 'home');
          this.$store.commit('setGuestDetail', {});
          this.$store.commit('setBidDetail', {});
          this.$store.commit('setNewBiddingPrice', {});
          this.$store.commit('setSelectedRoom', {});
          this.$store.commit('setBidStatus', null);
          this.$store.commit('setBidCountOffer', null);
          this.$store.commit('setFinalBidPriceMsg', null);
          this.$store.commit('setBookingId', null);
          this.$store.commit('setBookingDetail', null);
          this.$store.commit('setBookingUserEmail', null);
          this.$store.commit('setOrderAcceptedMail', null);
          this.$store.commit('setPaymentToken', null);
          this.$store.commit('setSaferPayUrl', null);
          this.$store.commit('setGroupId', 0);
          this.$store.commit('setIsHome', 1);
          this.$store.commit('setPreviousBidAmt', 0);
          this.$store.commit('setServiceType', 0);
          this.$store.commit('setDayUseServiceCheckinHours', {});
          this.$store.commit('setLimitExceedRoom', 0);
          this.$store.commit('setAcceptOnlinePayment', 0);
          this.$store.commit('setDayUseServiceGroupCheckinHours', {});
          this.$store.commit('setCancelMsg', null);
          this.$store.commit('setBannerUseType', 2);

         //Locale set in nuxt
          this.$i18n._vm.locale = this.$store.getters.getLanguage
      }
      
  }

}