import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import {Validator} from 'vee-validate'

Vue.use(Vuex);

export const state = () => ({
    meta: null,
    allNews: [],
    hotel: null,
    domain: null,
    latestNews: null,
    landingpage: null,
    domainStyle: null,
    domainDetails: null,
    isDeactive: 0,
    bookingHistory:{},
    userDetail:{},
    activePage: null,
    token:null,
    currency: null,
    language: null,
    withoutOffer: null,
    bookNow: null,
    bidDetail: {},
    selectedBidRoom: {},
    searchCriteria: {},
    guestDetail: {},
    bidStatus: null,
    bidCountOffer: null,
    newBiddingPrice: {},
    finalBidPriceMsg: null,
    bookingId: null,
    bookingDetail: null,
    bookingUserEmail: null,
    orderAcceptedMail: null,
    paymentToken: null,
    saferPayUrl: null,
    groupId: 0,
    isHome: 0,
    isCancel: 1,
    homeBanner: null,
    previousBidAmt:0,
    serviceType:0,
    dayUseServiceCheckinHours:{},
    limitExceedRoom:0,
    pLimitExceedRoom:null,
    dayUseServiceGroupCheckinHours:{},
    acceptOnlinePayment:0,
    cancelMsg:null,
    homeDayBanner:null,
    bannerUseType:2,
    reloadCount:0
})

export const mutations = {
  setWBHotelDetails (state, payload) {
            // state.meta = payload.meta;
            state.landingpage = Object.keys(payload.landingpage).length > 0 ? payload.landingpage[0] : null;
            state.domain = payload.subdomain.domainType;
            state.hotel = payload.subdomain.details.hotel;
            state.domainStyle = payload.style;
            state.homeBanner = payload.homeBanner;
            state.homeDayBanner = payload.homeDayBanner;
            
            if(state.domain == 'group') {
                if(state.currency == null) state.currency = payload.subdomain.details.currency_code;
                if(state.language == null) state.language = payload.subdomain.details.lang_code !== null ? payload.subdomain.details.lang_code : 'en';
            } else {
                if(state.currency == null) state.currency = state.hotel.currency_code;
                if(state.language == null) state.language = state.hotel.language_id !== null ? state.hotel.language_id : 'en';
                this.commit('setLanguage', state.language);
                this.commit('setCurrency', state.hotel.currency_code);
            }
            
            //state.currency = state.hotel.currency_code;
            
            // Setting the default language
            // state.language = state.landingpage !== null ? state.landingpage.lang_code : 'en';
            
            // Remove hotel key from details

            state.domainDetails = payload.subdomain.details;
            state.isDeactive = payload.subdomain.details ? 0 : 1;
            delete payload.subdomain.details.hotel;
        },
        
        setBookingHistory (state,payload) {
            state.bookingHistory.upcomingData = payload.upcoming;
            state.bookingHistory.completedData = payload.completed;
            state.bookingHistory.cancelledData = payload.cancelled;
        },
        
        setIsDeactive (state,payload) {
            state.isDeactive = payload
        },

        setUserDetail (state,payload) {
            state.userDetail = payload
        },

        setActivePage (state, payload) {
            state.activePage = payload;
        },

        setToken(state, payload){
            state.token = payload;
        },

        setCurrency (state, currency) {
            state.currency = currency;
        },

        setLanguage (state, language) { 
            state.language = language;
            
            // Set current language as default language and render it
            Vue.config.lang = language;
            import(`vee-validate/dist/locale/${language}`).then(locale => {
                Validator.localize(language, locale);
            });
        },

        setDefaultCurrencyAndLanguage (state) {
            state.currency = null;
            state.language = null;
        },
        
        setSelectedRoom (state, rooms) {
            state.selectedBidRoom = rooms;
        },
        
        setWithoutOffer (state, withoutOffer) {
            state.withoutOffer = withoutOffer;
        },

        setBookNow (state, bookNow) {
            state.bookNow = bookNow;
        },

        setBidDetail (state, bidDetail) {
            state.bidDetail = bidDetail;
        },

        setSearchCriteria (state, criteria) {
            state.searchCriteria = criteria;
        },

        setGuestDetail (state, guestDetail) {
            state.guestDetail = guestDetail;
        },

        setBidStatus (state, bidStatus) {
            state.bidStatus = bidStatus;
        },

        setBidCountOffer (state, bidCountOffer) {
            state.bidCountOffer = bidCountOffer;
        },

        setNewBiddingPrice (state, newBiddingPrice) {
            state.newBiddingPrice = newBiddingPrice;
        },

        setFinalBidPriceMsg (state, finalBidPriceMsg) {
            state.finalBidPriceMsg = finalBidPriceMsg;  
        },

        setBookingId (state, bookingId) {
            state.bookingId = bookingId;
        },
        
        setBookingDetail (state, bookingDetail) {
            state.bookingDetail = bookingDetail;
        },
        
        setBookingUserEmail (state, bookingUserEmail) {
            state.bookingUserEmail = bookingUserEmail;
        },
        
        setOrderAcceptedMail (state, orderAcceptedMail) {
            state.orderAcceptedMail = orderAcceptedMail;
        },
        
        setPaymentToken (state, paymentToken) {
            state.paymentToken = paymentToken;
        },

        setSaferPayUrl (state, url) {
            state.saferPayUrl = url;
        },

        setGroupId (state, groupId) {
            state.groupId = groupId;
        },

        setIsHome (state, isHome) {
            state.isHome = isHome;
        },

        setIsCancel (state, isCancel) {
            state.isCancel = isCancel;
        },

        setBanner (state, homeBanner) {
            state.homeBanner = homeBanner;
        },
        
        resetState(state) {
            Object.assign(state , JSON.parse(JSON.stringify(initialStateCopy)))
            // store.replaceState(JSON.parse(JSON.stringify(initialStateCopy)))
        },
        
        resetHomePageState(state) {
            Object.assign(state , JSON.parse(JSON.stringify(initialHomeStateCopy)))
        },
        setPreviousBidAmt(state,previousBidAmt) {
          state.previousBidAmt = previousBidAmt
      },
      setLimitExceedRoom(state,limitExceedRoom){
        state.limitExceedRoom = limitExceedRoom
    },

    setPLimitExceedRoom(state,pLimitExceedRoom){
        state.pLimitExceedRoom = pLimitExceedRoom
    },

    setServiceType(state,serviceType){
        state.serviceType = serviceType
    },

    setDayUseServiceCheckinHours(state,dayUseServiceCheckinHours) {
        state.dayUseServiceCheckinHours = dayUseServiceCheckinHours
    },

    setDayUseServiceGroupCheckinHours(state,dayUseServiceGroupCheckinHours) {
        state.dayUseServiceGroupCheckinHours = dayUseServiceGroupCheckinHours
    },
    setAcceptOnlinePayment(state,acceptOnlinePayment) {
        state.acceptOnlinePayment = acceptOnlinePayment
    },
    setCancelMsg(state,cancelMsg) {
        state.cancelMsg = cancelMsg
    },

    setDayBanner (state, homeDayBanner) {
        state.homeDayBanner = homeDayBanner;
    },
    setBannerUseType(state,bannerUseType){
        state.bannerUseType = bannerUseType
    },
    reloadCount(state,reloadCount){
        state.reloadCount = reloadCount
    }
}

export const actions = {
	setWBHotelDetails (state, payload) {
		
            // state.meta = payload.meta;
            state.landingpage = Object.keys(payload.landingpage).length > 0 ? payload.landingpage[0] : null;
            state.domain = payload.subdomain.domainType;
            state.hotel = payload.subdomain.details.hotel;
            state.domainStyle = payload.style;
            state.homeBanner = payload.homeBanner;
            state.homeDayBanner = payload.homeDayBanner;
            
            if(state.domain == 'group') {
                if(state.currency == null) state.currency = payload.subdomain.details.currency_code;
                if(state.language == null) state.language = payload.subdomain.details.lang_code !== null ? payload.subdomain.details.lang_code : 'en';
            } else {
                if(state.currency == null) state.currency = state.hotel.currency_code;
                if(state.language == null) state.language = state.hotel.language_id !== null ? state.hotel.language_id : 'en';
                this.commit('setLanguage', state.language);
                this.commit('setCurrency', state.hotel.currency_code);
            }
            
            //state.currency = state.hotel.currency_code;
            
            // Setting the default language
            // state.language = state.landingpage !== null ? state.landingpage.lang_code : 'en';
            
            // Remove hotel key from details

            state.domainDetails = payload.subdomain.details;
            state.isDeactive = payload.subdomain.details ? 0 : 1;
            delete payload.subdomain.details.hotel;
        }
    }

    export const getters = {
        hotel: state => state.hotel,
        meta: state => state.meta,
        allNews: state => state.allNews,
        domain: state => state.domain,
        latestNews: state => state.latestNews,
        landingpage: state => state.landingpage,
        domainStyle: state => state.domainStyle,
        domainDetails: state => state.domainDetails,
        isDeactive: state => state.isDeactive,
        bookingHistory: state => state.bookingHistory,
        userDetail: state => state.userDetail,
        activePage: state => state.activePage,
        token: state => state.token,
        getCurrency: state => state.currency,
        getLanguage: state => state.language,
        getWithoutOffer: state => state.withoutOffer,
        getBookNow: state => state.bookNow,
        getBidDetail: state => state.bidDetail,
        selectedBidRoom: state => state.selectedBidRoom,
        getSearchCriteria: state => state.searchCriteria,
        getGuestDetail: state => state.guestDetail,
        getBidStatus: state => state.bidStatus,
        getBidCountOffer: state => state.bidCountOffer,
        getNewBiddingPrice: state => state.newBiddingPrice,
        getFinalBidPriceMsg: state => state.finalBidPriceMsg,
        getBookingId: state => state.bookingId,
        getBookingDetail: state => state.bookingDetail,
        getBookingUserEmail: state => state.bookingUserEmail,
        getOrderAcceptedMail: state => state.orderAcceptedMail,
        getPaymentToken: state => state.paymentToken,
        getSaferPayUrl: state => state.saferPayUrl,
        getGroupId: state => state.groupId,
        getIsHome: state => state.isHome,
        getIsCancel: state => state.isCancel,
        getBanner: state => state.homeBanner,
        getPreviousBidAmt: state => state.previousBidAmt,
        getServiceType: state => state.serviceType,
        dayUseServiceCheckinHours: state => state.dayUseServiceCheckinHours,
        getLimitExceedRoom:state => state.limitExceedRoom,
        getPLimitExceedRoom:state => state.pLimitExceedRoom,
        dayUseServiceGroupCheckinHours: state => state.dayUseServiceGroupCheckinHours,
        acceptOnlinePayment: state => state.acceptOnlinePayment,
        cancelMsg: state => state.cancelMsg,
        getDayBanner: state => state.homeDayBanner,
        getBannerUseType: state => state.bannerUseType,
        reloadCount:state => state.reloadCount,
    }

    export const plugins = [createPersistedState()]

const initialStateCopy = {
    meta: null,
    allNews: [],
    hotel: null,
    domain: null,
    latestNews: null,
    landingpage: null,
    domainStyle: null,
    domainDetails: null,
    isDeactive:0,
    bookingHistory:{},
    userDetail:{},
    activePage: null,
    token:null,
    currency: null,
    language: null,
    withoutOffer: null,
    bookNow: null,
    bidDetail: {},
    selectedBidRoom: {},
    searchCriteria: {},
    guestDetail: {},
    bidStatus: null,
    bidCountOffer: null,
    newBiddingPrice: {},
    finalBidPriceMsg: null,
    bookingId: null,
    bookingDetail: null,
    bookingUserEmail: null,
    orderAcceptedMail: null,
    paymentToken: null,
    saferPayUrl: null,
    groupId: 0,
    isHome: 0,
    isCancel: 1,
    homeBanner: null,
    previousBidAmt:0,
    serviceType:0,
    dayUseServiceCheckinHours:{},
    limitExceedRoom:null,
    pLimitExceedRoom:null,
    dayUseServiceGroupCheckinHours:{},
    acceptOnlinePayment:0,
    cancelMsg:null,
    homeDayBanner: null,
    bannerUseType:2,
    reloadCount:0
}

    const initialHomeStateCopy = {
        currency: null,
        language: null,
        isCancel: 1
    }

