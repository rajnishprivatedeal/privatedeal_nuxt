import axios from 'axios'

export default function({ $axios, store }) {
  if (process.client) {
    $axios.defaults.baseURL = window.location.origin+'/api'
  }

  $axios.interceptors.request.use((config) => {
       
        config.headers['Content-Type'] = 'application/json';
        var token = store.getters.token;
        // var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9mdGhvdGVsYm9yZGVhdXgucHZ0ZGVhbC5jb20vYXBpL2xvZ2luL2VuIiwiaWF0IjoxNjE1MzA4MTM3LCJleHAiOjE2MTUzOTQ1MzcsIm5iZiI6MTYxNTMwODEzNywianRpIjoiV1JSQmtYVkZWSEZSb2NPVyIsInN1YiI6MjE0LCJwcnYiOiJjNzIxMmU0OGY5ZjRiNGRmMjY0YzU2MTMwMTZkYjdkMmU5ZmRkNTFmIn0.gSOKW-sWkV47QVPpQVVb0U_K3-MMCWKddlNsgSqCU2o"
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token; 
        }
        return config ;
    } , (err) => {
        return Promise.reject(err);
    });


    $axios.interceptors.response.use((response) => {
        if (response.data.code && response.data.code.code == 500) {
            // bus.$toastr.e(bus.$t('message.SESSION_EXPIRED'));
            this.$store.commit('resetState');
            serverBus.$emit('logout' , 'logout');
            this.$router.push({path : '/'});
        } else {
            return response;
        }
    } , (err) => {
        return Promise.reject(err);
    });
}