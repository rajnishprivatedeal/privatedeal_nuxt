import axios from 'axios';
// import Toastr from 'vue-toastr'
import Vue from 'vue';
import router from '@/router.js'
import VueI18n from 'vue-i18n'
// import store  from '@/store/index.js';
import { HotelDetails } from '@/mixins/hotel-details.js';

import { serverBus } from '@/main';

let bus = new Vue();
// Vue.use(Toastr)
Vue.use(VueI18n)

export default function interseptorSetup() {
    mixins: [ HotelDetails ],

    axios.interceptors.request.use((config) => {
       
        config.headers['Content-Type'] = 'application/json';
        var token = this.$store.state.token;
        // var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9mdGhvdGVsYm9yZGVhdXgucHZ0ZGVhbC5jb20vYXBpL2xvZ2luL2VuIiwiaWF0IjoxNjE1MzA4MTM3LCJleHAiOjE2MTUzOTQ1MzcsIm5iZiI6MTYxNTMwODEzNywianRpIjoiV1JSQmtYVkZWSEZSb2NPVyIsInN1YiI6MjE0LCJwcnYiOiJjNzIxMmU0OGY5ZjRiNGRmMjY0YzU2MTMwMTZkYjdkMmU5ZmRkNTFmIn0.gSOKW-sWkV47QVPpQVVb0U_K3-MMCWKddlNsgSqCU2o"
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token; 
        }
        return config ;
    } , (err) => {
        return Promise.reject(err);
    });


    axios.interceptors.response.use((response) => {
        if (response.data.code && response.data.code.code == 500) {
            // bus.$toastr.e(bus.$t('message.SESSION_EXPIRED'));
            this.$store.commit('resetState');
            serverBus.$emit('logout' , 'logout');
            this.$router.push({path : '/'});
        } else {
            return response;
        }
    } , (err) => {
        return Promise.reject(err);
    });
}