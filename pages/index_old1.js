import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/home/Index.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  // base: __dirname,
  routes: [
    /*-----routing for search hotel rooms------*/
    {
      path: '/',
      name: 'welcome',
      component: Welcome
    }
  ]
})


components: {
  dirs: [
    '~/components',
      {
        path: '~/components/header',
        prefix: 'header'
      },
      {
        path: '~/components/footer',
        prefix: 'footer'
      },
      {
        path: '~/components/home',
        prefix: 'index'
      },
      {
        path: '~/components/search',
        prefix: 'search'
      }
  ]
},





,
  router: { 
    base: '/',
    routeNameSplitter: '/',
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'route',
        path: '*',
        component: resolve(__dirname, 'route/index.js')
      })
    }
  }