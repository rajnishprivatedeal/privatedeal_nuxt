// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
// import App from './App'
import axios from 'axios'
import router from '@/router.js'
// import Vuetify from 'vuetify'
// import Toastr from 'vue-toastr'
// import { store } from '@/store'
import VueAxios from 'vue-axios'
import VModal from 'vue-js-modal'
import VeeValidate from 'vee-validate'
import VueInternalization from 'vue-i18n'
import Locales from '@/localization.json'
import { Constants } from '@/config/constants.js'
import moment from 'moment'

Vue.config.productionTip = Constants.productionTip;

// Requiring styles
// require('../static/app.css');
// require('vue-toastr/src/vue-toastr.scss');


// import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css'
// import 'font-awesome/css/font-awesome.min.css'
require('@/static/style.css');
require('@/static/media.css');

// Requiring moment js/ lodash
window.moment = require('moment');
window._ = require('lodash');

// Interceptors
import interceptorSetup from './interceptors/interceptor';
interceptorSetup();

// Registering plugin
Vue.use(VeeValidate, { locale: Constants.defaultLanguage, dictionary: Constants.VeeValidateDictionary})
// Vue.use(Toastr)
// Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueInternalization)
Vue.use(VModal, { dialog: true })
Vue.use(Vuex);

Vue.axios.defaults.baseURL = Constants.apiEndPoint
// Vue.axios.setBaseURL(window.location.host);
// Setting default language
Vue.config.lang = Constants.defaultLanguage;

// Object.keys(Locales).forEach(function (lang) {
//   // Vue.locale(lang, Locales[lang])
// });
export const serverBus = new Vue();
/* eslint-disable no-new */
// new Vue({
  // el: '#app',
  // store,
  // router,
  // VueInternalization,
  // components: { App },
   // template: '<App/>'
// });
